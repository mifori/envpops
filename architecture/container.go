package architecture

import (
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/lists"
	"gitlab.com/mifori/envpops/moduls"
	"gitlab.com/mifori/envpops/so"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/tools"
)

//Container instala y almacena variables para un contenedor
func Container() {
	console.Println(console.DirLang("architecture", "lists/container"))

	selection := tools.SelectOption(lists.Arch("container"))

	if !so.IsInstall(selection) {
		moduls.ModulTools([]string{selection})
	}

	moduls.ModulArchitecture([]string{selection})

	config := structs.GetConfig()
	config.Architecture.Container = selection
	structs.SetConfig(config)
}
