package architecture

import (
	"fmt"

	"gitlab.com/mifori/envpops/architecture/backend"
	"gitlab.com/mifori/envpops/architecture/components"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/lists"
	"gitlab.com/mifori/envpops/moduls"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/tools"
)

var installer = map[string]func(string){
	"automation": components.Automation,
	"database":   components.Database,
	"databaseWP": components.Database,
	"git":        components.Git,
	"proxy":      components.Proxy,

	"gestor": backend.Gestor,
}

var depend = map[string][]string{
	"git":        []string{"proxy"},
	"automation": []string{"proxy"},
	"backend":    []string{"proxy"},
	"wordpress":  []string{"databaseWP"},
}

//Add  genera una infraestructura completa
func Add(arch string) {
	moduls.ModulArchitectureLang()
	depends := depend[arch]

	props := structs.GetConfig()
	container := props.Architecture.Container

	if container == "" {
		Container()
	}

	for i := 0; i < len(depends); i++ {
		confirmation(depends[i])
	}

	create(container, arch)
}

//create  crea la infraestructura solicitada
func create(container string, arch string) {
	fmt.Println("")
	console.ShowLangln("architecture", "lists/"+arch)

	selection := tools.SelectOption(lists.Arch(arch))

	if lists.Arch(selection) != nil {
		create(arch, selection)
	} else {
		depends := depend[selection]

		for i := 0; i < len(depends); i++ {
			confirmation(depends[i])
		}

		installer[arch](selection)
	}
}

//confirmation  crea la infraestructura solicitada si el usuario acepta
func confirmation(arch string) {
	config := structs.GetConfig()

	stored := map[string]string{
		"proxy":      config.Architecture.Proxy,
		"databaseWP": containsStored(arch, config.Databases),
	}

	if stored[arch] == "" {
		console.ShowLang("architecture", "install/"+arch)
		confirmation := tools.Scanf()

		if confirmation == "s" || confirmation == "y" {
			Add(arch)
		}
	}
}

func containsStored(arch string, arr []string) string {
	listArch := lists.Arch(arch)

	for _, a := range arr {
		if tools.Contains(listArch, a) {
			return a
		}
	}

	return ""
}
