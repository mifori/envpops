package backend

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/mifori/envpops/architecture/programs"
	"gitlab.com/mifori/envpops/architecture/programs/proxy"
	"gitlab.com/mifori/envpops/config"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/files"
	"gitlab.com/mifori/envpops/system/tools"
	"golang.org/x/crypto/ssh/terminal"
)

//Gestor instala un gestor de información
func Gestor(selection string) {
	ip := proxy.IP("front")

	configG(selection, ip)
	proxy.Router(selection, ip)
}

// configG  busca el gestor de contenido a instalar
func configG(program string, ip string) {
	arch := "gestor"

	configuration := structs.GetConfig()
	network := structs.GetNetwork()

	//Carpetas que tienen los archivos a implementar
	folder := os.Getenv("POPSFOLDER") + "/" + arch + "/" + program
	dir := os.Getenv("POPSDATA") + "/architecture/" + configuration.Architecture.Container + "/" + program

	//Agregamos las variables de entorno en un archivo .env
	textFile := "POPS_SRC=" + folder + "\n"
	textFile += "POPS_FRONT=" + network.IP.Front + "\n"
	textFile += "POPS_IP=" + ip + "\n"

	if config.DATABASE["server"] != "" {
		textFile = dbActual(textFile)
	} else {
		console.JSON("database", program)

		console.JSON("database", "host")
		host := tools.Scanf()
		textFile += "POPS_HOST=" + host + "\n"
		config.DATABASE["host"] = strings.Split(host, ":")[0]

		console.JSON("database", "database")
		db := tools.Scanf()
		textFile += "POPS_DB=" + db + "\n"

		console.JSON("database", "nombre")
		user := tools.Scanf()
		textFile += "POPS_USER=" + user + "\n"

		console.JSON("database", "clave")
		password, err := terminal.ReadPassword(0)
		if err != nil {
			fmt.Println("Password error")
			os.Exit(0)
		} else {
			fmt.Println("")
		}

		clave := string(password)
		textFile += "POPS_PASSWORD=" + clave + "\n"
	}

	if program == "wordpress" {
		if configuration.Server.HTTPS {
			textFile += "POPS_EXTRA=$_SERVER['HTTPS'] = 'on';\n"
		} else {
			textFile += "POPS_EXTRA=\n"
		}
	}

	files.WriteVariables(dir+"/.env", textFile)
	programs.RunS(program, folder, dir)

	if config.DATABASE["server"] != "" {
		console.JSONln("wait", config.DATABASE["server"]+"_"+program)
	}
}

//dbActual  anexa la configuración actual de la base de datos
func dbActual(text string) string {
	data := config.DATABASE
	server := data["server"]

	config.DATABASE["host"] = "back.org"
	text += "POPS_HOST=back.org:" + console.ReadJSONValue(console.DirL("architecture", "port"), server) + "\n"

	text += "POPS_DB=" + data["db"] + "\n"
	text += "POPS_USER=" + data["user"] + "\n"
	text += "POPS_PASSWORD=" + data["clave"] + "\n"

	return text
}
