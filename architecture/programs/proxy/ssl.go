package proxy

import (
	"os"

	"gitlab.com/mifori/envpops/config"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/system/cmdi"
	"gitlab.com/mifori/envpops/system/execi"
	"gitlab.com/mifori/envpops/system/tools"
)

// GenerateDNS  configurar dns
func GenerateDNS() (string, bool, bool) {
	console.JSON("ssh", "asignar")
	confirmation := tools.Scanf()

	var dns string
	https := false
	cert := false
	provider := ""

	if confirmation == "s" || confirmation == "y" {
		console.JSON("ssh", "nombre")
		dns = tools.Scanf()

		console.JSON("ssh", "https")
		confirmation = tools.Scanf()

		if confirmation == "s" || confirmation == "y" {
			https = true
			config.ConfVars["https"] = "true"

			console.ShowLangln("architecture", "lists/cert")
			cert, provider = entitySSL(dns)
		}
	}

	config.ConfVars["domain"] = dns
	config.ConfVars["provider"] = provider

	return dns, https, cert
}

//entitySSL selecciona una entidad ssl
func entitySSL(dns string) (bool, string) {
	console.JSON("all", "select")
	opc := tools.Scanf()

	switch opc {
	case "1":
		POPSPATH := os.Getenv("POPSPATH")
		DATA := os.Getenv("POPSDATA")
		SERVER := os.Getenv("POPSSERVER")

		key := POPSPATH + "/cert/host"

		if config.MODE == "dev" {
			key = "../" + DATA + "/cert/host"
		}

		execi.Install("openssl")
		execi.Install("cert", "--domain", dns, "--key", key)

		if _, err := os.Stat(POPSPATH + "/host.pem"); os.IsNotExist(err) {
			cmdi.DownloadFileProd(SERVER+"cert/host.pem", POPSPATH+"/cert", "host.pem")
		}

		return true, "local"
	case "2":
		execi.Install("certbot")
		return false, "certbot"
	case "3":
		return false, ""
	default:
		entitySSL(dns)
		return false, "external"
	}
}
