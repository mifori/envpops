package proxy

import (
	"os"

	"gitlab.com/mifori/envpops/architecture/programs"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/moduls"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/cmdi"
	"gitlab.com/mifori/envpops/system/files"
)

//proxys lista de programas de automatizacion
var proxys = map[string]func(string, string){
	"nginx": RouterNginx,
}

//Router configura el proxy para el programa determinado
func Router(program string, ip string) {
	proxy := structs.GetConfig().Architecture.Proxy

	if proxy != "" {
		proxys[proxy](program, ip)
	}
}

//Replace reemplaza puertos en los archivos de configuracion del proxy
func Replace(program string, old string, news string) {
	proxy := structs.GetConfig().Architecture.Proxy

	if proxy == "" {
		return
	}

	cmdi.Exec("find", os.Getenv("POPSFOLDER")+"/proxy/"+proxy, "-name", program+"*", "-type", "f", "-exec",
		"sed", "-i", "s/"+old+"/"+news+"/g", "{}", "+",
	)
}

//RouterNginx  alamcenando ruta nginx
func RouterNginx(program string, ip string) {
	config := structs.GetConfig()
	dns := config.Server.Domain

	dst := os.Getenv("POPSFOLDER") + "/proxy/nginx"
	pathAll := os.Getenv("POPSDATA") + "/architecture/proxy/nginx/"
	path := pathAll + program

	if _, err := os.Stat(pathAll); os.IsNotExist(err) {
		moduls.ModulArchitecture([]string{"proxy"})
	}

	if dns != "" {
		if config.Server.HTTPS {
			cmdi.Copyi(path+"/server_https.conf", dst+"/conf/"+program+".conf")
			cmdi.Copyi(path+"/stream_https.conf", dst+"/stream/"+program+".conf")
		} else {
			cmdi.Copyi(path+"/server_http.conf", dst+"/conf/"+program+".conf")
			cmdi.Copyi(path+"/stream_http.conf", dst+"/stream/"+program+".conf")
		}

		files.ReplaceInDir(dst, "dev.pops", dns)
	} else {
		cmdi.Copyi(path+"/server.conf", dst+"/conf/"+program+".conf")
		cmdi.Copyi(path+"/stream.conf", dst+"/stream/"+program+".conf")
	}

	files.ReplaceInFilei(
		dst+"/conf/"+program+".conf",
		"000.00.0.0",
		ip,
	)

	files.ReplaceInFilei(
		dst+"/stream/"+program+".conf",
		"000.00.0.0",
		ip,
	)

	programs.Runf(console.Dir("architecture/proxy/nginx/commands/" + config.Architecture.Container))
}
