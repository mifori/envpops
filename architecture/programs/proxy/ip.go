package proxy

import (
	"strconv"
	"strings"

	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/cmdi"
)

//containers lista de programas de automatizacion
var containers = map[string]func(string) string{
	"docker": IPDocker,
}

//IP busca una ip disponible
func IP(red string) string {
	config := structs.GetConfig()
	return containers[config.Architecture.Container](red)
}

//IPDocker busca una ip disponible en docker
func IPDocker(red string) string {
	network := structs.GetNetwork()
	out := cmdi.ExecG("docker", "ps", "-a", "-q")

	command := strings.Fields(out)
	ipSp := network.IP.Front_SP

	if red == "back" {
		ipSp = network.IP.Back_SP
	} else if red == "git" {
		ipSp = network.IP.Git_SP
	}

	ip := strconv.FormatInt(ipSp[0], 10) + "." +
		strconv.FormatInt(ipSp[1], 10) + "." +
		strconv.FormatInt(ipSp[2], 10) + "."

	if len(command) == 0 {
		return ip + strconv.FormatInt(ipSp[3]+1, 10)
	}

	cmd := []string{"inspect", "-f",
		"\"{{ .NetworkSettings.Networks.pops_" + red + ".IPAddress }}\""}
	cmd = append(cmd, command...)
	out = cmdi.ExecG("docker", cmd...)

	num := ipSp[3]
	ipValid := ip + strconv.FormatInt(num, 10)

	for {
		num++
		ipValid = ip + strconv.FormatInt(num, 10)
		constains := strings.Contains(out, ipValid)

		if !constains {
			break
		}
	}

	return ipValid
}

//NextIP devuelve la ip continua
func NextIP(ip string) string {
	numbers := strings.Split(ip, ".")

	newip := numbers[0] + "." +
		numbers[1] + "." +
		numbers[2] + "."

	num, err := strconv.ParseInt(numbers[3], 10, 64)

	if err != nil {
		return ""
	}

	return newip + strconv.FormatInt(num+1, 10)
}
