package git

import (
	"os"

	gitconfig "github.com/tcnksm/go-gitconfig"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/so"
	"gitlab.com/mifori/envpops/system/execi"
	"gitlab.com/mifori/envpops/system/tools"
)

//Config  configura las variables git
func Config() {
	execi.Install("git")

	username, _ := gitconfig.Username()
	email, _ := gitconfig.Email()

	if username == "" {
		console.JSON("git", "name")
		nameScan := tools.Scan()

		execi.CommandShow("git", "config", "--global", "user.name", nameScan)
	}

	if email == "" {
		console.JSON("git", "email")
		emailScan := tools.Scanf()

		execi.CommandShow("git", "config", "--global", "user.email", emailScan)
	}
}

//GenerateRSA  crea una llave ssh si no existe
func GenerateRSA() string {
	rsa := so.Home() + "/.ssh/id_rsa"
	rsaPub := rsa + ".pub"

	if _, err := os.Stat(rsaPub); os.IsNotExist(err) {
		execi.CommandShow("ssh-keygen", "-t", "rsa", "-f", rsa)
	}

	return rsaPub
}
