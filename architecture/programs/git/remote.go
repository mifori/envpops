package git

import (
	"fmt"

	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/system/execi"
	"gitlab.com/mifori/envpops/system/tools"
)

//Remote  guarda la ubicacion del repositorio remoto
func Remote() string {
	console.JSON("git", "remoto")
	repositorio := tools.Scanf()

	rsa := GenerateRSA()

	fmt.Println("")
	console.JSON("git", "ssh")

	execi.CommandShow("cat", rsa)

	fmt.Println("")
	console.JSON("git", "cargar")

	tools.Scanf()

	return repositorio
}
