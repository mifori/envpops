package git

import (
	"log"
	"os"

	"gitlab.com/mifori/envpops/architecture/programs"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/so"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/cmdi"
	"gitlab.com/mifori/envpops/system/files"
	"gitlab.com/mifori/envpops/system/tools"
)

//Local  instala un sistema de servidor git
func Local(ip string) (string, string, string) {
	configuration := structs.GetConfig()
	repositorio := os.Getenv("POPSFOLDER")
	container := configuration.Architecture.Container
	domain := configuration.Server.Domain
	yml := ""

	cmdi.MkdirAll(repositorio + "/keys")
	cmdi.MkdirAll(repositorio + "/repos")
	GenerateRSA()

	if _, err := os.Stat(repositorio + "/keys/id_rsa.local.pub"); os.IsNotExist(err) {
		cmdi.Copy(
			so.Home()+"/.ssh/id_rsa.pub",
			repositorio+"/keys/id_rsa.local.pub",
		)
	}

	dir := os.Getenv("POPSDATA") + "/architecture/" + container + "/git"

	includeText := "POPS_FOLDER=" + repositorio + "\n"
	includeText += "IP=" + ip + "\n"

	portLocal := ""
	portPrivate := tools.Scaner("architecture", "gitPrivate", "port")
	includeText += "PORT_PRIVATE=" + portPrivate + "\n"

	if domain == "" {
		portLocal = tools.Scaner("architecture", "gitPublic", "port")
		includeText += "PORT_PUBLIC=" + portLocal + "\n"
		yml = "proxy"
	}

	files.WriteVariables(dir+"/.env", includeText)

	errMove := files.CopyFiles(dir, repositorio)

	if errMove != nil {
		log.Fatal(errMove)
	}

	programs.Exec(
		console.Dir("architecture/"+container+"/commands/run"),
		[]string{"--folder", "--dir", "--yml"},
		repositorio, dir, yml,
	)

	cmdi.Remove("-r", repositorio+"/gitserver")
	return repositorio, portLocal, portPrivate
}
