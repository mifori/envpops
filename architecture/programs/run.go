package programs

import (
	"os"

	"gitlab.com/mifori/envpops/config"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/execi"
)

//Run corre un archivo sh de la arquitectura
func Run(args ...string) {
	configuration := structs.GetConfig()
	file := os.Getenv("POPSDATA") + "/architecture/" + configuration.Architecture.Container + "/commands/run"

	keys := []string{"--folder", "--dir"}
	Exec(file, keys, args...)
}

//RunS envia comando stop
func RunS(args ...string) {
	configuration := structs.GetConfig()
	file := os.Getenv("POPSDATA") + "/architecture/" + configuration.Architecture.Container + "/commands/run"

	keys := []string{"--stop", "--folder", "--dir", "--container"}
	Exec(file, keys, args...)
}

//Runf ejecuta un archivo .sh
func Runf(file string, args ...string) {
	keys := []string{"--folder", "--dir", "--container"}
	Exec(file, keys, args...)
}

//Runrm ejecuta un archivo .sh
func Runrm(file string, args ...string) {
	keys := []string{"--remove"}
	Exec(file, keys, args...)
}

//Exec corre un archivo sh de la arquitectura
func Exec(file string, keys []string, args ...string) {
	file = file + ".sh"
	params := []string{file}

	if config.PROJECT != "" {
		params = append(params, "--project", config.PROJECT)
	}

	for i := 0; i < len(args); i++ {
		params = append(params, keys[i], args[i])
	}

	_, err := execi.CommandShow("bash", params...)

	if err != nil {
		os.Exit(0)
	}
}
