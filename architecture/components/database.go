package components

import (
	"fmt"
	"os"
	"strconv"

	"gitlab.com/mifori/envpops/architecture/programs"
	"gitlab.com/mifori/envpops/config"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/cmdi"
	"gitlab.com/mifori/envpops/system/files"
	"gitlab.com/mifori/envpops/system/tools"
	"golang.org/x/crypto/ssh/terminal"
)

//Database  Instala una base de datos
func Database(selection string) {
	configDB(selection)

	configuration := structs.GetConfig()
	configuration.Databases = append(configuration.Databases, selection)
	structs.SetConfig(configuration)
}

//configDB  instala el contenedor de BD
func configDB(program string) {
	arch := "database"

	configuration := structs.GetConfig()
	network := structs.GetNetwork()
	version := tools.ScanerVersion("architecture", program)

	//Carpetas que tienen los archivos a implementar
	folder := os.Getenv("POPSFOLDER") + "/" + arch + "/" + program
	dir := os.Getenv("POPSDATA") + "/architecture/" + configuration.Architecture.Container + "/" + arch

	//Agregamos las variables de entorno en un arhivo .env
	textFile := "POPS_FOLDER=" + folder + "/data\n"
	textFile += "POPS_NAME=" + config.PROJECT + "_" + program + "\n"
	textFile += "POPS_RED=" + network.Proxy.RED_BACK + "\n"
	textFile += "POPS_VERSION=" + version + "\n"

	//Solicitamos la informacion necesaria para base de datos
	console.JSONln("database", "titulo")

	console.JSON("database", "database")
	db := tools.Scanf()
	textFile += "POPS_DB=" + db + "\n"

	console.JSON("database", "nombre")
	user := tools.Scanf()
	textFile += "POPS_USER=" + user + "\n"

	console.JSON("database", "clave")
	password, err := terminal.ReadPassword(0)
	if err != nil {
		fmt.Println("Password error")
		os.Exit(0)
	} else {
		fmt.Println("")
	}

	clave := string(password)
	textFile += "POPS_PASSWORD=" + clave + "\n"

	console.JSON("database", "exponer")
	confirmation := tools.Scanf()

	if confirmation == "s" || confirmation == "y" {
		port := tools.ScanerPort("architecture", program)
		textFile += "POPS_PORT=" + port + "\n"
	}

	files.WriteVariables(dir+"/.env", textFile)

	//Creamos y damos permisos a la carpeta de informacion
	cmdi.MkdirAll(folder + "/data")
	cmdi.Chown("-R",
		strconv.Itoa(os.Getuid())+":"+strconv.Itoa(os.Getgid()),
		folder+"/data",
	)

	if confirmation == "s" || confirmation == "y" {
		programs.RunS(program, folder, dir, program)
	} else {
		programs.Exec(
			console.Dir("architecture/"+configuration.Architecture.Container+"/commands/run"),
			[]string{"--stop", "--container", "--folder", "--dir", "--yml"},
			program, program, folder, dir, "withoutPort",
		)
	}

	//Guardamos las variables de base de datos
	config.DATABASE["server"] = program
	config.DATABASE["db"] = db
	config.DATABASE["user"] = user
	config.DATABASE["clave"] = clave
}
