package components

import (
	"log"
	"os"
	"strconv"

	"gitlab.com/mifori/envpops/architecture/programs"
	"gitlab.com/mifori/envpops/architecture/programs/proxy"
	"gitlab.com/mifori/envpops/config"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/files"
	"gitlab.com/mifori/envpops/system/tools"
)

//Proxy configura un proxy
func Proxy(selection string) {
	confP(selection)

	configuration := structs.GetConfig()

	configuration.Architecture.Proxy = selection
	configuration.Server.Domain = config.ConfVars["domain"]
	configuration.Server.HTTPS, _ = strconv.ParseBool(config.ConfVars["https"])
	configuration.Server.Provider = config.ConfVars["provider"]

	structs.SetConfig(configuration)
}

//confP configura el entorno proxy a trabajar
func confP(program string) {
	arch := "proxy"

	configuration := structs.GetConfig()
	dns, _, cert := proxy.GenerateDNS()
	network := structs.GetNetwork()
	version := tools.ScanerVersion("architecture", program)

	//Carpetas que tienen los archivos a implementar
	dir := os.Getenv("POPSDATA") + "/architecture/" + configuration.Architecture.Container + "/" + program
	folder := os.Getenv("POPSFOLDER") + "/" + arch + "/" + program

	//Agregamos las variables de entorno en un archivo .env
	textFile := "POPS_FOLDER=" + folder + "\n"
	textFile += "POPS_FOLDER_LOG=" + folder + "/log\n"
	textFile += "POPS_NAME=" + config.PROJECT + "_" + program + "\n"
	textFile += "POPS_VERSION=" + version + "\n"
	files.WriteInclude(dir+"/.env", textFile, &network.Proxy)

	programs.RunS(program, folder, dir)

	if dns != "" {
		if cert {
			err := files.CopyFiles("./cert", folder+"/ssl")

			if err != nil {
				log.Fatal(err)
			}

			os.RemoveAll("./cert")
		}

		files.ReplaceInDir(folder, "dev.pops", dns)
	}
}
