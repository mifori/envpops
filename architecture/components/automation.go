package components

import (
	"os"
	"strconv"

	"gitlab.com/mifori/envpops/architecture/programs"
	"gitlab.com/mifori/envpops/architecture/programs/proxy"
	"gitlab.com/mifori/envpops/config"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/cmdi"
	"gitlab.com/mifori/envpops/system/files"
	"gitlab.com/mifori/envpops/system/tools"
)

// Automation  instala un contenedor a utilizar
func Automation(selection string) {
	ip := proxy.IP("back")

	configA(selection, ip)
	proxy.Router(selection, ip)

	configParams := structs.GetConfig()
	configParams.Architecture.Automation = selection
	structs.SetConfig(configParams)
}

//configA  configurar un sistema de automatizacion
func configA(program string, ip string) {
	arch := "automation"

	configuration := structs.GetConfig()
	network := structs.GetNetwork()
	yml := ""

	container := configuration.Architecture.Container
	domain := configuration.Server.Domain

	//Carpetas que tienen los archivos a implementar
	folder := os.Getenv("POPSFOLDER") + "/" + arch + "/" + program
	dir := os.Getenv("POPSDATA") + "/architecture/" + container + "/" + program

	//Creamos y damos permisos a la carpeta de informacion
	cmdi.MkdirAll(folder + "/data")
	cmdi.Chown("-R",
		strconv.Itoa(os.Getuid())+":"+strconv.Itoa(os.Getgid()),
		folder+"/data",
	)

	//Agregamos las variables de entorno en un arhivo .env
	//textFile := files.ReadVariables(&network.Automation)
	textFile := ""
	textFile += "POPS_FOLDER=" + folder + "/data\n"
	textFile += "POPS_NAME=" + config.PROJECT + "_" + program + "\n"
	textFile += "POPS_BACK=" + network.IP.Back + "\n"
	textFile += "POPS_RED=" + network.Proxy.RED_BACK + "\n"
	textFile += "POPS_IP=" + ip + "\n"

	if domain == "" {
		port := tools.Scaner("architecture", program, "port")
		textFile += "PORT=" + port + "\n"

		if program == "jenkins" {
			port = tools.Scaner("architecture", "jenkinsApi", "port")
			textFile += "PORT_API=" + port + "\n"
		}

		yml = "proxy"
	}

	files.WriteInclude(dir+"/.env", textFile, &network.Proxy)

	programs.Exec(
		console.Dir("architecture/"+container+"/commands/run"),
		[]string{"--folder", "--dir", "--yml"},
		folder, dir, yml,
	)
}
