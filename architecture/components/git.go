package components

import (
	"gitlab.com/mifori/envpops/architecture/programs/git"
	"gitlab.com/mifori/envpops/architecture/programs/proxy"
	"gitlab.com/mifori/envpops/structs"
)

// Git  instala un contenedor a utilizar
func Git(selection string) {
	git.Config()
	src := ""

	switch selection {
	case "local":
		ip := proxy.IP("git")

		repositorio, portPublic, portPrivate := git.Local(ip)
		src = repositorio

		proxy.Router("git", ip)

		proxy.Replace("git", "0000", portPublic)
		proxy.Replace("git", "1111", portPrivate)
	case "remote":
		src = git.Remote()
	}

	configParams := structs.GetConfig()

	configParams.Repository = src
	configParams.RepositoryType = selection

	structs.SetConfig(configParams)
}
