mkdir .tmp/
MYPWD=$(pwd)/.tmp

cd data

for dir in `ls`
do
  if test -f "$dir"; then
    cp $dir $MYPWD/${dir}
  fi

  test -d "$dir" || continue

  mkdir -p ${MYPWD}/${dir}
  cd $dir

  for subdir in `ls`
  do
    if test -f "$subdir"; then
      cp ${subdir} ${MYPWD}/${dir}/${subdir}
    fi

    test -d "$subdir" || continue
    echo "$subdir"

    zip -r ${subdir}.zip $subdir
    mv ${subdir}.zip ${MYPWD}/${dir}/${subdir}.zip
  done

  cd ..
done

cd ..

zip -r data.zip .tmp/*
rm -r .tmp
