package commands

import (
	"os"
	"strings"

	"gitlab.com/mifori/envpops/architecture"
	"gitlab.com/mifori/envpops/architecture/programs"
	"gitlab.com/mifori/envpops/config"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/cmdi"
	"gitlab.com/mifori/envpops/validations"
)

// Git  despliega los sistemas necesarios para crear un sistema git
func Git(args []string) {
	permitted := []string{"--help"}
	commands, commandsArgs, initial, _ := validations.Check(args, permitted)

	path := os.Getenv("POPSPATH") + "/.gitpops"
	os.Setenv("POPSFOLDER", path)
	config.PROJECT = "git"

	for i := 0; i < len(commands); i++ {
		gitCommand(commands[i])
	}

	if len(initial) == 0 {
		console.PrintI18nExitf("git/help", os.Getenv("POPSPROGRAM"))
	}

	if c := structs.GetConfig(); c.RepositoryType != "local" && initial[0] != "install" {
		console.JSONExit("git", "not")
	}

	gitActions(initial, commandsArgs)
}

//gitActions  realiza las acciones permitidas por git
func gitActions(words []string, commandsArgs map[string][]string) {
	switch words[0] {
	case "install":
		path := os.Getenv("POPSFOLDER")

		if _, err := os.Stat(path); os.IsNotExist(err) {
			cmdi.Mkdir(path)
		}

		architecture.Add("git")
	case "add":
		add(words, commandsArgs)
	case "rm":
		remove(words, commandsArgs)
	case "run":
		runCommand(words, commandsArgs)
	default:
		console.JSONExit("git", "unknown")
	}
}

//gitCommand  realiza las acciones relacionadas a un comando
func gitCommand(command string) {
	switch command {
	case "--help":
		console.PrintI18nExitf("git/help", os.Getenv("POPSPROGRAM"))
	case "-p":
		return
	default:
		console.JSONExit("git", "unknown")
	}
}

//add  crea los repositorios
func add(words []string, commands map[string][]string) {
	typeF := "public"

	private := commands["-p"]
	if private != nil {
		typeF = "private"
		words = append(words, private...)
	}

	if len(words) == 1 {
		console.PrintI18nExit("git/add")
	}

	if len(commands) > 1 {
		console.JSONExit("git", "unknown")
	}

	folder := os.Getenv("POPSFOLDER") + "/"
	folders := words[1:]

	for i := 0; i < len(folders); i++ {
		f := strings.ToLower(folders[i])

		if _, err := os.Stat(folder + "repos/" + f + ".git"); !os.IsNotExist(err) {
			console.JSONfln("git", "exist", f)
			continue
		}

		configuration := structs.GetConfig()
		container := configuration.Architecture.Container

		programs.Exec(
			console.Dir("architecture/"+container+"/commands/git"),
			[]string{"--" + typeF},
			f,
		)
	}
}

//remove  elimina los repositorios
func remove(words []string, commands map[string][]string) {
	if len(words) == 1 {
		console.PrintI18nExit("git/rm")
	}

	if len(commands) > 0 {
		console.JSONExit("git", "unknown")
	}

	folders := words[1:]
	folder := os.Getenv("POPSFOLDER")

	for i := 0; i < len(folders); i++ {
		f := strings.ToLower(folders[i])

		cmdi.ExecE("rm", "-r",
			folder+"/repos/"+f+".git",
		)
	}
}

//runCommand  ejecuta un comando dentro del contenedor git
func runCommand(words []string, commands map[string][]string) {
	if len(words) == 1 {
		console.PrintI18nExit("git/command")
	} else if len(words) > 2 {
		console.JSONExit("git", "unknown")
	}

	if len(commands) > 0 {
		console.JSONExit("git", "unknown")
	}

	cmdi.ExecShow("docker", "exec", "-i", "git_server", "sh", "-c", "\""+words[1]+"\"")
}
