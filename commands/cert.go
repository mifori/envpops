package commands

import (
	"log"
	"os"

	"gitlab.com/mifori/envpops/config"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/system/cmdi"
	"gitlab.com/mifori/envpops/system/execi"
	"gitlab.com/mifori/envpops/system/files"
	"gitlab.com/mifori/envpops/system/tools"
)

//SSLCertificate configura el entorno proxy a trabajar
func SSLCertificate(args []string) {
	console.JSON("ssh", "nombre")
	dns := tools.Scanf()

	POPSPATH := os.Getenv("POPSPATH")
	DATA := os.Getenv("POPSDATA")
	SERVER := os.Getenv("POPSSERVER")

	key := POPSPATH + "/cert/host"

	if config.MODE == "dev" {
		key = "../" + DATA + "/cert/host"
	}

	execi.Install("openssl")
	execi.Install("cert", "--domain", dns, "--key", key)

	if _, err := os.Stat(POPSPATH + "/host.pem"); os.IsNotExist(err) {
		cmdi.DownloadFileProd(SERVER+"cert/host.pem", POPSPATH+"/cert", "host.pem")
	}

	err := files.CopyFiles("./cert", POPSPATH+"/cert/"+dns)

	if err != nil {
		log.Fatal(err)
	}

	os.RemoveAll("./cert")
}
