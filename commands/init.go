package commands

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/mifori/envpops/architecture"
	"gitlab.com/mifori/envpops/architecture/programs"
	"gitlab.com/mifori/envpops/config"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/lists"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/cmdi"
	"gitlab.com/mifori/envpops/system/files"
	"gitlab.com/mifori/envpops/system/tools"
	"gitlab.com/mifori/envpops/validations"
)

var actionsInit = []string{"proxy", "backend"}

// Init  crea un nuevo proyecto
func Init(args []string) {
	commands, commandsArgs, initial := validations.Extract(args)
	err := validations.Validate(commands, []string{"--remove", "--services", "--help"})

	if err != "" {
		console.JSONExitf("init", "unknown", err)
	}

	if len(commands) > 0 && commands[0] == "--help" {
		console.PrintI18nExitf("help/init", os.Getenv("POPSPROGRAM"))
	}

	if len(initial) == 0 {
		console.JSONExit("init", "init")
	}

	nombre := strings.Join(initial, " ")

	if len(commands) == 0 {
		fist(nombre, false)
		actions(actionsInit)
		return
	}

	switch commands[0] {
	case "--services":
		services := commandsArgs["--services"]

		if len(services) == 0 {
			console.PrintI18nExit("help/services")
		}

		fist(nombre, true)
		actions(services)
	case "--remove":
		removes := commandsArgs["--remove"]

		if len(removes) == 0 {
			console.PrintI18nExit("help/rm")
		}

		src := strings.ToLower(strings.Replace(nombre, " ", "_", -1))
		path := os.Getenv("POPSPATH") + "/" + src

		if _, err := os.Stat(path); os.IsNotExist(err) {
			console.JSONExit("init", "not_remove")
		}

		config.PROJECT = src
		os.Setenv("POPSFOLDER", path)

		rm(removes)
	}
}

// InitName  crea un nuevo proyecto preguntando por un nombre al usuario
func InitName() {
	nombre := getName()
	fist(nombre, false)
	actions(actionsInit)
}

//getName  solicita el nombre del proyecto a inicializar
func getName() string {
	console.JSON("init", "nombre")
	nombre := tools.Scan()
	return nombre
}

func actions(services []string) {
	var list = make(map[string]bool)
	length := len(services)
	exists := lists.Components

	for i := 0; i < length; i++ {
		if !exists[services[i]] {
			console.JSONExitf("init", "desconocido", services[i])
		}

		list[services[i]] = true
	}

	for key := range exists {
		if list[key] && exists[key] {
			architecture.Add(key)
		}
	}

	last()
}

//rm elimina un programa del proyecto
func rm(list []string) {
	length := len(list)
	listPrograms := lists.Programs
	configuration := structs.GetConfig()

	for i := 0; i < length; i++ {
		if !listPrograms[list[i]] {
			console.JSONExitf("init", "program", list[i])
		}
	}

	for i := 0; i < length; i++ {
		program := list[i]

		programs.Runrm(console.Dir("architecture/"+configuration.Architecture.Container+"/commands/remove"), config.PROJECT+"_"+program)

		cmdi.Exec("find", os.Getenv("POPSFOLDER"), "-name", program, "-type", "d", "-exec",
			"rm", "-r", "-f", "{}", "+",
		)

		cmdi.Exec("find", os.Getenv("POPSFOLDER"), "-name", "*"+program+"*", "-type", "f", "-exec",
			"rm", "{}", "+",
		)

		files.ReplaceInFilei(
			os.Getenv("POPSFOLDER")+"/"+os.Getenv("POPSPROGRAM")+".json",
			program,
			"",
		)
	}
}

//fist  inicia con el proceso de despliegue
func fist(nombre string, change bool) {
	src := strings.ToLower(strings.Replace(nombre, " ", "_", -1))
	path := os.Getenv("POPSPATH") + "/" + src

	_, err := os.Stat(path)

	if !os.IsNotExist(err) && !change {
		console.JSONExit("init", "existe")
	}

	config.PROJECT = src
	os.Setenv("POPSFOLDER", path)

	if os.IsNotExist(err) {
		cmdi.MkdirAll(path)

		conf := structs.GetConfigGlobal()
		configParams := structs.GetConfig()

		configParams.Project = src
		configParams.Name = nombre
		configParams.Repository = conf.Repository + "/" + src
		configParams.Version = "0.0.1"

		structs.SetConfig(configParams)
	}
}

//last  muestra acciones para el usuario
func last() {
	conf := structs.GetConfig()
	text := ""

	if conf.Server.HTTPS {
		if conf.Server.Provider == "local" {
			text += fmt.Sprintf(getText("copia"), os.Getenv("POPSFOLDER"))
			text += fmt.Sprintf(getText("hosts"), conf.Server.Domain)
		} else if conf.Server.Provider == "letsencrypt​" {
			text += fmt.Sprintf(getText("letsencrypt"), os.Getenv("POPSFOLDER"))
		} else {
			text += getText("external")
		}
	}

	if text == "" {
		return
	}

	contenido := getText("recuerda")
	contenido += text
	contenido += getText("entiendo")

	fmt.Print(contenido)
	tools.Scan()
}

func getText(value string) string {
	values := console.ReadJSON(console.DirI18n("language", "messages/init"))
	text := fmt.Sprintf("%v", values[value])

	return text
}
