package commands

import (
	"os"
	"strings"

	"gitlab.com/mifori/envpops/architecture/programs"
	"gitlab.com/mifori/envpops/config"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/cmdi"
	"gitlab.com/mifori/envpops/validations"
)

//Deleted  crea un nuevo proyecto
func Deleted(args []string) {
	commands, _, initial := validations.Extract(args)

	if len(commands) > 0 || len(initial) == 0 {
		console.PrintI18nExitf("help/remove", os.Getenv("POPSPROGRAM"))
	}

	nombre := strings.Join(initial, " ")
	src := strings.ToLower(strings.Replace(nombre, " ", "_", -1))
	path := os.Getenv("POPSPATH") + "/" + src

	if _, err := os.Stat(path); os.IsNotExist(err) {
		console.JSONExit("init", "not_remove")
	}

	config.PROJECT = src
	os.Setenv("POPSFOLDER", path)

	container := structs.GetConfig().Architecture.Container
	programs.Exec(
		console.Dir("architecture/"+container+"/commands/remove"),
		[]string{"--remove"},
		src+"_",
	)

	cmdi.Remove("-r", path)
}
