package commands

import (
	"gitlab.com/mifori/envpops/config"
	"gitlab.com/mifori/envpops/console"
)

//Version   Muestra en consola la version actual
func Version(args []string) {
	console.JSONe("all", "version", config.VERSION)
}
