package so

import (
	"runtime"

	"gitlab.com/mifori/envpops/so/linux"
)

//Install  verifica el sistema y llama a la funcion install del mismo
func Install(exec func(string, ...string) (string, error), program string, params ...string) string {
	switch runtime.GOOS {
	case "linux":
		return linux.Install(exec, program, params...)
	default:
		return "not"
	}
}

//IsInstall  verifica que un programa este instalado
func IsInstall(program string) bool {
	switch runtime.GOOS {
	case "linux":
		return linux.IsInstall(program)
	default:
		return false
	}
}

/*
Go111osAix
GoosAndroid
GoosDarwin
GoosDragonfly
GoosFreebsd
GoosHurd
GoosJs
GoosLinux
GoosNacl
GoosNetbsd
GoosOpenbsd
GoosPlan9
GoosSolaris
GoosWindows
GoosZos
*/
