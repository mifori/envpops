package so

import (
	"log"
	"runtime"

	"gitlab.com/mifori/envpops/so/linux"
)

//Conf devuelve la configuracion aplicada para determinado elemento
func Conf(params ...string) string {
	config := make(map[string]func() string)
	actions := make(map[string]func(string) string)

	//acciones a realizar dependiendo del sistema operativo
	switch runtime.GOOS {
	case "linux":
		config = linux.GetConfig()
		actions = linux.GetActions()
	}

	length := len(params)

	if length == 0 {
		log.Fatalln("so.conf: Se necesita saber la funcion a llamar")
	} else if length == 1 {
		return config[params[0]]()
	}

	return actions[params[0]](params[1])
}

//Home retorna el home del sistema operativo
func Home() string {
	return Conf("home")
}
