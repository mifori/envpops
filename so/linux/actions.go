package linux

import (
	"os"
	"syscall"
)

var config = map[string]func() string{
	"home":       home,
	"path":       path,
	"permission": Permission,
}

//GetConfig devuelve los parametros de configuracion
func GetConfig() map[string]func() string {
	return config
}

//home  Devuelve la carpeta home del sistema
func home() string {
	home := os.Getenv("XDG_CONFIG_HOME")

	if home == "" {
		home = os.Getenv("HOME")
	}

	return home
}

//path  Devuelve la carpeta donde se configurara el programa
func path() string {
	return "/etc/" + os.Getenv("POPSPROGRAM")
}

//Permission  Devuelve el comando de permisos root
func Permission() string {
	euid := syscall.Getuid()

	if euid != 0 {
		return "sudo"
	}

	return ""
}
