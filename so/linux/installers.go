package linux

import (
	"os"
	"os/exec"
	"strings"
)

//Install  inicia el codigo de instalacion previamente guardado en un .sh
func Install(
	command func(string, ...string) (string, error),
	program string,
	s ...string,
) string {
	split := strings.Split(program, "/")
	last := split[len(split)-1]
	file := ""

	// comprueba si el programa ya esta instalado
	if IsInstall(last) {
		return "ok"
	}

	//busca archivo de instalacion
	if split[0] == "tools" || split[0] == "moduls" {
		file = os.Getenv("POPSDATA") + "/" + program + "/installer/linux.sh"
	} else {
		file = os.Getenv("POPSDATA") + GetFile(program)
	}

	if _, err := os.Stat(file); os.IsNotExist(err) {
		return file
	}

	//ejecuta archivo de instalacion
	params := append([]string{file}, s...)
	command("bash", params...)

	return "ok"
}

//IsInstall  verifica si un programa esta instalado
func IsInstall(pkg string) bool {
	split := strings.Split(pkg, "/")
	pkg = split[len(split)-1]

	path, _ := exec.LookPath(pkg)

	if path == "" {
		return false
	}

	return true
}
