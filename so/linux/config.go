package linux

import "os/user"

var actions = map[string]func(string) string{
	"getFile": GetFile,
	"group":   PermissionGroup,
}

//GetActions devuelve las acciones que puede realizar
func GetActions() map[string]func(string) string {
	return actions
}

//GetFile  devuelve la ruta del archivo de instalacion
func GetFile(program string) string {
	return "/os/linux/" + program + ".sh"
}

//PermissionGroup  Devuelve el comando sudo en caso que el user no este en el grupo
func PermissionGroup(group string) string {
	_, err := user.LookupGroup(group)

	if err != nil {
		return Permission()
	}

	return ""
}
