package moduls

import (
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/system/execi"
)

var dependTools = map[string][]string{
	"kubeadm": []string{"docker"},
}

//ModulTools instala una herramienta
func ModulTools(args []string) {
	name := "tools"
	folder := name + "/"

	ModulDownload(folder + "__lang")

	_, _, initial := ModulCommand(name, args, []string{"--help"})

	for i := 0; i < len(initial); i++ {
		ModulDownload(folder + initial[i])
	}

	for i := 0; i < len(initial); i++ {
		tool := initial[i]
		depends := dependTools[tool]

		for j := 0; j < len(depends); j++ {
			ModulDownload(folder + depends[j])
			execi.Install(folder + depends[j])
			console.ShowI18n(folder+depends[j]+"/lang", "installed")
		}

		execi.Install(folder + tool)
		console.ShowI18n(folder+tool+"/lang", "installed")
	}
}
