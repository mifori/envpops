package moduls

import (
	"os"
	"strings"

	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/system/cmdi"
	"gitlab.com/mifori/envpops/validations"
)

//ModulVerify verifica que un modulo exista
func ModulVerify(modul string, args []string) {
	for i := 0; i < len(args); i++ {
		exist := false
		path := os.Getenv("POPSDATA") + "/" + modul + "/" + args[i]

		if _, err := os.Stat(path); !os.IsNotExist(err) {
			exist = true
		}

		if !exist {
			ModulPrint(modul, "unknown", args[i])
			os.Exit(0)
		}
	}
}

//ModulCommand verifica los comandos permitidos en el modulo
func ModulCommand(modul string, args []string, commandsVR []string) ([]string, map[string][]string, []string) {
	commands, commandsArgs, initial := validations.Extract(args)
	validations.Validate(commands, commandsVR)

	if (len(commands) > 0 && commands[0] == "--help") || len(initial) == 0 {
		ModulPrint(modul, "help", os.Getenv("POPSPROGRAM"))
		os.Exit(0)
	}

	return commands, commandsArgs, initial
}

//ModulDownload descarga un modulo
func ModulDownload(modul string) {
	split := strings.Split(modul, "/")
	folder := split[0]
	name := split[1]

	if os.Getenv("POPSMODE") == "dev" {
		if _, err := os.Stat(os.Getenv("POPSDATA") + "/" + modul); os.IsNotExist(err) {
			ModulPrint(folder, "unknown", name)
			os.Exit(0)
		}

		return
	}

	src := os.Getenv("POPSCONF") + "/" + modul

	if _, err := os.Stat(src); os.IsNotExist(err) {
		stderr := cmdi.DownloaderZip(modul, src)

		if stderr != nil {
			ModulPrint(folder, "unknown", name)
			os.Exit(0)
		}
	}
}

//ModulPrint  muestra un mensaje en la consola de las herramientas
func ModulPrint(modul string, message string, data string) {
	src := console.DirLang(modul, message)
	console.Printf(src, data)
}
