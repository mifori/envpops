package moduls

var dependArch = map[string][]string{}

//ModulArchitecture instala una herramienta
func ModulArchitecture(args []string) {
	name := "architecture"
	folder := name + "/"

	ModulDownload(folder + "__lang")

	_, _, initial := ModulCommand(name, args, []string{})

	for i := 0; i < len(initial); i++ {
		ModulDownload(folder + initial[i])
	}

	for i := 0; i < len(initial); i++ {
		tool := initial[i]
		depends := dependArch[tool]

		for j := 0; j < len(depends); j++ {
			ModulDownload(folder + depends[j])
		}
	}
}

//ModulArchitectureLang descarga el modulo de lenguage
func ModulArchitectureLang() {
	name := "architecture"
	folder := name + "/"
	ModulDownload(folder + "__lang")
}
