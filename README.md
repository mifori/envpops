# EnvPops

EnvPops es un sistema de despliegue de aplicaciones automatizadas y pre-configuradas.

Nuestro propósito es proporcionar una forma rápida y sencilla de generar sistemas estructurados, escalables y con buenas practicas. Sin tener que ser el proyecto mas grande o contar con el apoyo de múltiples expertos para poder implementarlo.

## Instalación

### Instalar con un archivo de comandos

Para instalar envpops solo hay que llamar al archivo de instalación y ejecutar el mismo

```
curl https://envpops.io/data/installer.sh | bash -s
```

La variable $POPSPATH es opcional y es la carpeta donde se guardan los proyectos que por defecto es $HOME/containers, si se desea cambiar esta ruta solo hay que agregar la variable en el comando de instalación.

```
curl https://envpops.io/data/installer.sh --path $POPSPATH | bash -s
```

Recuerda cambiar $POPSPATH por la ruta especifica.

### Instalar desde archivo ZIP

- Descarga el zip desde envpops

```
wget https://envpops.io/ar/installers/envpops-v0.2.4-linux-x64.zip
unzip envpops-v0.2.4-linux-x64.zip
rm envpops-v0.2.4-linux-x64.zip
```

- Define envpops como un comando global

```
chmod a+x envpops
sudo mv envpops /usr/local/bin
```

- Indica en donde se guardan tus proyectos, por defecto se guardan en $HOME/envpops

```
echo 'export POPSPATH=<YOUR_FOLDER>' >> ~/.bashrc
source ~/.bashrc
```

## Commandos

| Comando       | Descripción                                  |
| ------------- | -------------------------------------------- |
| init          | Instala y configura contenedores             |
| rm            | Elimina un proyecto                          |
| git           | Configura un entorno git local               |
| install       | Instala herramientas en el sistema operativo |
| lang          | Cambia el idioma                             |
| cert          | Crea un certificado ssl local                |
| -v, --version | Mostrara la versión actual                   |

## Contenedores

- Docker

## Servicios

```
envpops init [project_name] --services [servicio]
```

### Proxy

- Nginx

En caso de configurar un sistema con certificados auto-firmados, la contraseña de la entidad de firma es **asdfgh**

### Automation

- Jenkins

### Database

- Mysql
- Mariadb
- Postgres
- Mongo

### Backend

- Gestor de contenido
  - Wordpress

## Herramientas

| Herramienta | Descripción                                       |
| ----------- | ------------------------------------------------- |
| docker      | Instala docker y docker-compose.                  |
| kubeadm     | Instala kubeadm y configura kubectl sobre docker. |
| terraform   | Instala terraform de forma global.                |

## Información

EnvPops es un proyecto de software libre.

Puedes apoyar el proyecto en:

- [Buy me an coffe](https://www.buymeacoffee.com/ingeniomaps)

O si eres conocedor de alguna tecnología y te gustaría ayudarnos a implementarla, copia el repositorio y ayúdanos con la integración.
