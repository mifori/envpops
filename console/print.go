package console

import (
	"fmt"
	"os"
)

//Print imprime un archivo
func Print(src string) {
	fmt.Print(Read(src))
}

//Println imprime un archivo
func Println(src string) {
	fmt.Println(Read(src) + "\n")
}

//Printf muestra el texto con parametros de un determinado archivo en consola
func Printf(src string, data string) {
	fmt.Printf(Read(src), data)
	fmt.Println("")
}

//PrintI18n imprime un archivo
func PrintI18n(src string) {
	fmt.Print(ReadI18n(src))
}

//PrintI18nln imprime un archivo
func PrintI18nln(src string) {
	fmt.Println(ReadI18n(src))
	fmt.Println("")
}

//PrintI18nf imprime un archivo
func PrintI18nf(src string, param string) {
	fmt.Printf(ReadI18n(src), param)
}

//PrintI18nExit imprime un archivo
func PrintI18nExit(src string) {
	fmt.Print(ReadI18n(src) + "\n")
	os.Exit(0)
}

//PrintI18nExitf imprime un archivo
func PrintI18nExitf(src string, param string) {
	fmt.Printf(ReadI18n(src)+"\n", param)
	os.Exit(0)
}
