package console

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
)

//Read  devuelve el mensaje dentro de un archivo .lang
func Read(src string) string {
	file, _ := ioutil.ReadFile(src + ".lang")
	return strings.TrimSpace(string(file))
}

//ReadI18n devuelve el mensaje dentro de un archivo .lang
func ReadI18n(src string) string {
	file, _ := ioutil.ReadFile(DirI18n("language", src) + ".lang")
	return strings.TrimSpace(string(file))
}

//ReadJSON leer un archivo json
func ReadJSON(src string) map[string]interface{} {
	var result map[string]interface{}

	file, _ := ioutil.ReadFile(src + ".json")

	_ = json.Unmarshal([]byte(string(file)), &result)

	return result
}

//ReadJSONValue imprime un mensaje y devuelve una respuesta del usuario
func ReadJSONValue(folder string, value string) string {
	texts := ReadJSON(folder)
	text := fmt.Sprintf("%v", texts[value])

	return text
}
