package console

import (
	"fmt"
)

//Show  muestra un mensaje en la consola
func Show(src string) {
	text := ReadI18n(src)
	fmt.Println(text + "\n")
}

//ShowI18n  muestra un mensaje en la consola
func ShowI18n(folder string, name string) {
	src := DirI18n(folder, name)
	text := Read(src)
	fmt.Println(text + "\n")
}

//ShowLang  muestra un mensaje en la consola
func ShowLang(folder string, name string) {
	src := DirLang(folder, name)
	text := Read(src)
	fmt.Print(text + " ")
}

//ShowLangln  muestra un mensaje en la consola
func ShowLangln(folder string, name string) {
	src := DirLang(folder, name)
	text := Read(src)
	fmt.Println(text + "\n")
}
