package console

import (
	"os"
	"strings"
)

//Dir devuelve la ruta de archivos
func Dir(file string) string {
	return os.Getenv("POPSDATA") + "/" + file
}

//DirL devuelve la ruta de archivos con __lang
func DirL(folder string, name string) string {
	return os.Getenv("POPSDATA") + "/" + folder + "/__lang/" + name
}

//DirI18n devuelve la ruta de archivos por idioma
func DirI18n(folder string, name string) string {
	path := os.Getenv("POPSDATA") + "/" + folder + "/" + os.Getenv("POPSLANG") + "/" + name
	return defaultLang(path)
}

//DirLang devuelve la ruta de archivos con __lang por idioma
func DirLang(folder string, name string) string {
	path := os.Getenv("POPSDATA") + "/" + folder + "/__lang/" + os.Getenv("POPSLANG") + "/" + name
	return defaultLang(path)
}

func defaultLang(path string) string {
	_, jsonErr := os.Stat(path + ".json")
	_, langErr := os.Stat(path + ".lang")

	if os.IsNotExist(jsonErr) && os.IsNotExist(langErr) {
		path = strings.Replace(path, "/"+os.Getenv("POPSLANG")+"/", "/"+os.Getenv("POPSLANGDEFAULT")+"/", -1)
	}

	return path
}
