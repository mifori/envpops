package console

import (
	"fmt"
	"os"
)

//JSON imprime el texto de un archivo json
func JSON(file string, value string) {
	fmt.Print(getText(file, value) + "")
}

//JSONf imprime el texto de un archivo json con parametros
func JSONf(file string, value string, s ...string) {
	fmt.Printf(getText(file, value)+" ", s[0])
	fmt.Println("")
}

//JSONln imprime el texto de un archivo json
func JSONln(file string, value string) {
	fmt.Println(getText(file, value))
}

//JSONfln imprime el texto de un archivo json
func JSONfln(file string, value string, s ...string) {
	fmt.Printf(getText(file, value), s[0])
	fmt.Println("")
}

//JSONe imprime el texto de un archivo json
func JSONe(file string, value string, s ...string) {
	fmt.Printf(getText(file, value), s[0])
	fmt.Println("")
	os.Exit(0)
}

//JSONExit imprime el texto de un archivo json
func JSONExit(file string, value string) {
	fmt.Printf(getText(file, value))
	fmt.Println("")
	os.Exit(0)
}

//JSONExitf imprime el texto de un archivo json
func JSONExitf(file string, value string, s ...string) {
	fmt.Printf(getText(file, value), s[0])
	fmt.Println("")
	os.Exit(0)
}

//getText busca el parametro dentro del json el cual imprimir
func getText(file string, value string) string {
	dir := DirI18n("language", "messages/"+file)
	values := ReadJSON(dir)

	text := fmt.Sprintf("%v", values[value])
	return text
}
