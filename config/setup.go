package config

import (
	"os"

	"gitlab.com/mifori/envpops/so"
	"gitlab.com/mifori/envpops/system/cmdi"
)

//setVariables Genera las variables de entorno necesarias
func setVariables() {
	os.Setenv("POPSMODE", MODE)
	os.Setenv("POPSPROGRAM", PROGRAM)

	os.Setenv("POPSDATA", VARIABLES[MODE]["data"])
	os.Setenv("POPSSERVER", VARIABLES[MODE]["server"])
	os.Setenv("POPSLANGDEFAULT", VARIABLES[MODE]["lang"])

	path := so.Conf("path")
	home := so.Home()

	if VARIABLES[MODE]["data"] == "PATH" {
		os.Setenv("POPSDATA", path)
	}

	cmdi.MkdirAll(path)
	cmdi.MkdirAll(home + "/." + PROGRAM)

	os.Setenv("POPSCONF", path)
	os.Setenv("POPSTEMP", home+"/."+PROGRAM)
}

//setPath verifica ruta del path donde guardar la informacion del proyecto
func setPath() {
	path := os.Getenv("POPSPATH")

	if path == "" {
		path = so.Home() + "/" + PROGRAM
		os.Setenv("POPSPATH", path)
	}

	cmdi.MkdirAll(path)
}

//setNetWork descarga el archivo de configuracion
func setNetWork() {
	POPSCONF := os.Getenv("POPSCONF") + "/"
	POPSSERVER := os.Getenv("POPSSERVER")
	file := "network.json"

	if _, err := os.Stat(POPSCONF + file); os.IsNotExist(err) {
		cmdi.DownloadFileProd(POPSSERVER+file, POPSCONF, file)
	}
}

//Setup configura lo requerido para el funcionamiento del programa
func Setup() {
	setVariables()
	setPath()

	//Revisa si esta conectado a internet
	cmdi.IsConnected()

	language()
	setNetWork()
}
