package config

//MODE   modo de ejecución del programa si es para desarrollo o producción
var MODE = "dev" // prod | dev

//PROGRAM   nombre del software
var PROGRAM = "envpops"

//VERSION   version del sistema actual
var VERSION = "0.2.4"

//VARIABLES   variables de entorno para funcionamiento local o de producción
var VARIABLES = map[string]map[string]string{
	"dev": {
		"server": "https://envpops.io/data/",
		"data":   "data",
		"lang":   "es",
	},

	"prod": {
		"server": "https://envpops.io/data/",
		"data":   "PATH",
		"lang":   "es",
	},
}

//LANGUAGES lista todos los idiomas configurados del sistema
var LANGUAGES = map[string]bool{
	"es": true,
	"en": true,
}

//LISTLANGUAGES lista de los lenguajes a seleccionar
var LISTLANGUAGES = []string{"es", "en"}

//FOLDER ruta de la carpeta donde se guarda la información
var FOLDER = ""

//PROJECT ruta de la carpeta donde se guarda el proyecto
var PROJECT = ""

//ConfVars son variables de configuración de algún programa
var ConfVars = make(map[string]string)

//DATABASE lista de base de datos
var DATABASE = make(map[string]string)

//Folders - linux
// Temporal: HOME() + /.envpops
// Configuración: /etc/envpops
// Proyectos: POPSPATH || HOME() + /envpops
// Archivo de programa global: /usr/local/bin
