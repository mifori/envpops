package config

import (
	"os"
	"strings"

	"gitlab.com/mifori/envpops/so"
	"gitlab.com/mifori/envpops/system/cmdi"
	"gitlab.com/mifori/envpops/system/execi"
)

//InstallG  instala el programa determinado con descarga de go
func InstallG(program string, commands ...string) {
	CONFPATH := os.Getenv("POPSCONF")
	POPSTEMP := os.Getenv("POPSTEMP")
	SERVER := os.Getenv("POPSSERVER")

	file := so.Conf("getFile", program)
	split := strings.Split(file, "/")
	name := split[len(split)-1]

	if _, err := os.Stat(CONFPATH + "/os/" + name); !os.IsNotExist(err) {
		so.Install(execi.CommandShow, program, commands...)
		return
	}

	folder := CONFPATH + "/os"
	if _, err := os.Stat(folder); os.IsNotExist(err) {
		cmdi.MkdirAll(folder)
	}

	cmdi.DownloadFileA(POPSTEMP+"/"+name, SERVER+file[1:])
	cmdi.Move(POPSTEMP+"/"+name, CONFPATH+"/os")

	so.Install(execi.CommandShow, program, commands...)
}
