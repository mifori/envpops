package config

import (
	"fmt"
	"os"
	"strings"

	"github.com/fatih/color"
	"gitlab.com/mifori/envpops/console"
	"gitlab.com/mifori/envpops/structs"
	"gitlab.com/mifori/envpops/system/cmdi"
	"gitlab.com/mifori/envpops/system/tools"
)

//language configura el lenguaje del programa
func language() {
	data := structs.GetConfigGlobal()

	if data.Language != "" {
		os.Setenv("POPSLANG", data.Language)
		return
	}

	// busca el lenguaje del sistema
	lang, _ := os.LookupEnv("LANG")
	lang = strings.Split(lang, ".")[0]

	if !LANGUAGES[lang] {
		lang = strings.Split(lang, "_")[0]

		if !LANGUAGES[lang] {
			lang = VARIABLES[MODE]["lang"]
		}
	}

	setLanguage(lang)
}

//setLanguage configura el idioma deseado
func setLanguage(lang string) {
	POPSTEMP := os.Getenv("POPSTEMP") + "/"
	CONFPATH := os.Getenv("POPSCONF") + "/"
	SERVER := os.Getenv("POPSSERVER")

	src := CONFPATH + "language"
	if _, err := os.Stat(src); !os.IsNotExist(err) {
		cmdi.RemoveFolder(src)
	}

	file := lang + ".zip"
	fmt.Printf("\r ... ")

	if MODE != "dev" {
		stderr := cmdi.DownloadFile(SERVER+"language/"+file, POPSTEMP, file)

		if stderr != nil {
			red := color.New(color.FgRed).SprintFunc()
			fmt.Printf("\r ... %s\n", red("ERROR"))
			fmt.Println(stderr)
			os.Exit(0)
		}

		cmdi.UnzipS(POPSTEMP+file, POPSTEMP)
		cmdi.MkdirAll(CONFPATH + "language")
		cmdi.Move(POPSTEMP+lang, CONFPATH+"language/"+lang)
		cmdi.RemoveFile(POPSTEMP + file)
	}

	data := structs.GetConfigGlobal()
	data.Language = lang
	structs.SetConfigGlobal(data)
	os.Setenv("POPSLANG", data.Language)

	green := color.New(color.FgGreen).SprintFunc()
	fmt.Printf("\r ... %s\n\n", green("OK"))
}

//ChangeLanguage permite al usuario seleccionar su propio idioma
func ChangeLanguage(args []string) {
	lang := selectLang()
	setLanguage(lang)
}

//selectLang  selecciona el idioma predeterminado
func selectLang() string {
	console.PrintI18nln("selection/langs")
	selection := tools.SelectOption(LISTLANGUAGES)

	return selection
}
