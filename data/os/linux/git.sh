SUDO=

if (( $EUID != 0 )); then
    SUDO='sudo'
fi

if [ -n "$(command -v yum)" ]; then
  ## Agregar IUS repo
  $SUDO yum -y install  https://centos7.iuscommunity.org/ius-release.rpm
  $SUDO yum -y install  git2u-all
elif [ -n "$(command -v apt-get)" ]; then
  $SUDO apt-get install -y git
elif [ -n "$(command -v apt)" ]; then
  $SUDO apt install -y git-all
elif [ -n "$(command -v dnf)" ]; then
  $SUDO dnf install -y git-all
elif [ -n "$(command -v make)" ]; then
  $SUDO make install -y git
fi
