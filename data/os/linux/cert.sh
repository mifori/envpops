# Comandos permitidos
while [ "$1" != "" ]; do
	case $1 in
		--domain )
			result="$@"
			result=${result//"$1 "/}
			IFS='-'; DOMAIN=($result); unset IFS;
			DOMAIN=${DOMAIN//" "/_}
			DOMAIN="$(cut -d'_' -f1 <<<"$DOMAIN")"
			;;
		--key )
			result="$@"
			result=${result//"$1 "/}
			IFS='-'; KEY=($result); unset IFS;
			KEY=${KEY//" "/_}
			KEY="$(cut -d'_' -f1 <<<"$KEY")"
			;;

    esac
    shift
done

mkdir -p cert
cd cert

### Generar entidad de firma
#openssl genrsa -des3 -out host.key 2048
#openssl req -x509 -new -nodes -key host.key -sha256 -days 10950 -out host.pem -subj "/C=CO/ST=Bogota/L=Bogota/O=EnvPops/OU=Departamento Desarrollo/CN=EnvPops Local Certificate"

### Generar .key .csr
openssl genrsa -out privkey.pem 2048
openssl req -new -key privkey.pem -out server.csr -subj "/C=CO/ST=Bogota/L=Bogota/O=EnvPops/OU=Departamento Desarrollo/CN=$DOMAIN"

### Crear .ext
cat > server.ext <<EOM
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = $DOMAIN
DNS.2 = *.$DOMAIN
DNS.3 = *.automation.$DOMAIN
EOM

### Generar certificado firmado
openssl x509 -req -in server.csr -CA ${KEY}.pem -CAkey ${KEY}.key -CAcreateserial -out fullchain.pem -days 1825 -sha256 -extfile server.ext

rm server.*
cd ..
