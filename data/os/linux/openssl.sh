SUDO=

if (( $EUID != 0 )); then
    SUDO='sudo'
fi

if [ -n "$(command -v yum)" ]; then
  $SUDO yum install -y openssl
elif [ -n "$(command -v apt-get)" ]; then
  $SUDO apt-get install -y openssl
elif [ -n "$(command -v apt)" ]; then
  $SUDO apt install -y openssl
elif [ -n "$(command -v dnf)" ]; then
  $SUDO dnf install -y openssl
elif [ -n "$(command -v make)" ]; then
  $SUDO make install -y openssl
fi
