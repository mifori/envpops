SUDO=

if (( $EUID != 0 )); then
    SUDO='sudo'
fi

if [ -n "$(command -v yum)" ]; then
  $SUDO yum install -y zip unzip
elif [ -n "$(command -v apt-get)" ]; then
  $SUDO apt-get install -y zip unzip
elif [ -n "$(command -v apt)" ]; then
  $SUDO apt install -y zip unzip
elif [ -n "$(command -v dnf)" ]; then
  $SUDO dnf install -y zip unzip
elif [ -n "$(command -v make)" ]; then
  $SUDO make install -y zip unzip
fi
