SUDO=

if (( $EUID != 0 )); then
    SUDO='sudo'
fi

# INSTALACION DOCKER
VERSION_COMPOSE=1.27.4


## Instalar Docker CE
#export VERSION=18.09
$SUDO curl -fsSL https://get.docker.com/ | sh
$SUDO usermod -a -G docker $USER


## Instalar Docker Compose
$SUDO curl -L "https://github.com/docker/compose/releases/download/${VERSION_COMPOSE}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$SUDO chmod +x /usr/local/bin/docker-compose
$SUDO ln -sfn /usr/local/bin/docker-compose /usr/bin/docker-compose

## Reestablecer servicio
if [ -n "$(command -v systemctl)" ]; then
  $SUDO systemctl stop docker
  $SUDO systemctl start docker
else
  $SUDO service docker stop
  $SUDO service docker start
fi
