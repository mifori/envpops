SUDO=

if (( $EUID != 0 )); then
    SUDO='sudo'
fi

if [ -n "$(command -v yum)" ]; then
  $SUDO yum install -y epel-release
  $SUDO yum install -y snapd
elif [ -n "$(command -v apt-get)" ]; then
  $SUDO apt-get install -y snapd
elif [ -n "$(command -v apt)" ]; then
  $SUDO apt install -y snapd
elif [ -n "$(command -v dnf)" ]; then
  $SUDO dnf install -y epel-release
  $SUDO dnf install -y snapd
elif [ -n "$(command -v make)" ]; then
  $SUDO make install -y snapd
fi

$SUDO ln -s /var/lib/snapd/snap /snap
