SUDO=
COMMAND=


if (( $EUID != 0 )); then
    SUDO='sudo'
fi


if [ -n "$(command -v dnf)" ]; then
  COMMAND=dnf
elif [ -n "$(command -v yum)" ]; then
  COMMAND=yum
elif [ -n "$(command -v apt-get)" ]; then
  COMMAND=apt-get
elif [ -n "$(command -v apt)" ]; then
  COMMAND=apt
fi


if [ $COMMAND == "apt-get" ] || [ $COMMAND == "apt" ]; then

  $SUDO $COMMAND update && $SUDO $COMMAND install -y apt-transport-https curl
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | $SUDO apt-key add -

cat <<-EOF | $SUDO tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF

  $SUDO $COMMAND update
  $SUDO $COMMAND install -y kubelet kubeadm kubectl
  $SUDO apt-mark hold kubelet kubeadm kubectl

fi


if [ $COMMAND == "dnf" ] || [ $COMMAND == "yum" ]; then

cat <<EOF | $SUDO tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

  # Set SELinux in permissive mode (effectively disabling it)
  $SUDO setenforce 0
  $SUDO sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

  $SUDO $COMMAND install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
  $SUDO systemctl enable --now kubelet

fi

$SUDO swapoff -a

#if [ `whoami` != root ]; then
#$SUDO su <<-HERE
#kubeadm init --apiserver-advertise-address=IP_SERVER --pod-network-cidr=192.168.0.0/16
#HERE
#else
#  kubeadm init --apiserver-advertise-address=IP_SERVER --pod-network-cidr=192.168.0.0/16
#fi

#mkdir -p $HOME/.kube
#$SUDO cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
#$SUDO chown $(id -u):$(id -g) $HOME/.kube/config

#$SUDO sysctl net.bridge.bridge-nf-call-iptables=1
#$SUDO kubectl apply -f https://docs.projectcalico.org/v3.11/manifests/calico.yaml
