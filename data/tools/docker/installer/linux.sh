SUDO=

if (($EUID != 0)); then
  SUDO='sudo'
fi

# Obtine la distribucion del sistema operativo
get_distribution() {
  lsb_dist=""

  # Every system that we officially support has /etc/os-release
  if [ -r /etc/os-release ]; then
    lsb_dist="$(. /etc/os-release && echo "$ID")"
  fi

  # Returning an empty string here should be alright since the
  # case statements don't act unless you provide an actual value
  echo "$lsb_dist"
}

lsb_dist=$(get_distribution)

# Instalacion DOCKER segun su distribucion
if [[ $lsb_dist == "almalinux" ]]; then
  $SUDO dnf install -y yum-utils
  $SUDO yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
  $SUDO dnf -y install docker-ce docker-ce-cli containerd.io
else
  ## Instalar Docker CE
  $SUDO curl -fsSL https://get.docker.com/ | sh
fi

$SUDO usermod -a -G docker $USER

## Instalar Docker Compose
VERSION_COMPOSE=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4)
$SUDO curl -L "https://github.com/docker/compose/releases/download/${VERSION_COMPOSE}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$SUDO chmod +x /usr/local/bin/docker-compose
$SUDO ln -sfn /usr/local/bin/docker-compose /usr/bin/docker-compose

## Actualizar estados
if [[ -f "~./profile" ]]; then
  source ~./profile
fi

if [[ -f "~./bash_profile" ]]; then
  source ~./bash_profile
fi

if [[ -f "~./bashrc" ]]; then
  source ~./bashrc
fi

## Reestablecer servicio
if [ -n "$(command -v systemctl)" ]; then
  $SUDO systemctl stop docker
  $SUDO systemctl start docker
else
  $SUDO service docker stop
  $SUDO service docker start
fi
