VERSION=0.12.23


if [ `arch` != x86_64 ]; then
  wget https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_linux_amd64.zip
  unzip terraform_${VERSION}_linux_amd64.zip
  rm terraform_${VERSION}_linux_amd64.zip
else
  wget https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_linux_386.zip
  unzip terraform_${VERSION}_linux_386.zip
  rm terraform_${VERSION}_linux_386.zip
fi


sudo mv ./terraform /usr/local/bin
