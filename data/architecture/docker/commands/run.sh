## Revisa si el usuario es admin
SUDO=

if (( $EUID != 0 )); then
    SUDO='sudo'
fi


## Se obtienen los parametros del usuario
while [ "$1" != "" ]; do
	case $1 in
    --dir )
				result="$@"
				result=${result//"$1 "/}
				IFS='-'; dir=($result); unset IFS;
        dir=${dir//" "/}
				;;

		--folder )
				result="$@"
				result=${result//"$1 "/}
				IFS='-'; folder=($result); unset IFS;
        folder=${folder//" "/}
				;;

		--project )
				result="$@"
				result=${result//"$1 "/}
				IFS='-'; project=($result); unset IFS;
				project=${project//" "/}
				;;

    --stop )
    		result="$@"
    		result=${result//"$1 "/}
    		IFS='-'; stop=($result); unset IFS;
    		stop=${stop//" "/}
    		;;

    --container )
    		result="$@"
    		result=${result//"$1 "/}
    		IFS='-'; container=($result); unset IFS;
    		container=${container//" "/}
    		;;

    --yml )
    		result="$@"
    		result=${result//"$1 "/}
    		IFS='-'; yml=($result); unset IFS;
    		yml=${yml//" "/}
    		;;
	esac
	shift
done

if [ "$yml" == "" ];
then
    yml=docker-compose
fi


## Copiar archivos de configuracion
if [ "$dir" != "" ];
then
    mkdir -p $folder
		cp -rT $dir $folder
fi


## Detiene los contenedores
if [ "$stop" != "" ];
then
    containers=$(docker ps -q --filter "name=_$stop")

    if [ "$containers" != "" ]; then
		  docker stop $containers > /dev/null 2>&1
    fi
fi


## Run codigo docker
if [ "$container" != "" ];
then
		docker-compose -f "$folder/$yml.yml" --env-file "$folder/.env" -p $project up -d $container
else
    docker-compose -f "$folder/$yml.yml" --env-file "$folder/.env" -p $project up -d
fi
