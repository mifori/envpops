## Se obtienen los parametros del usuario
while [ "$1" != "" ]; do
	case $1 in
    --remove )
				result="$@"
				result=${result//"$1 "/}
				IFS='-'; remove=($result); unset IFS;
        remove=${remove//" "/}
				;;
	esac
	shift
done


##Busco y elimino los contenedores deseados
containers=$(docker ps -a -q --filter "name=$remove" --format "{{.Names}}")

if [ "$containers" != "" ]; then
	docker rm -f $containers > /dev/null 2>&1
fi
