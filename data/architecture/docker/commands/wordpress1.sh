## Se obtienen los parametros del usuario
while [ "$1" != "" ]; do
	case $1 in
    --name )
				result="$@"
				result=${result//"$1 "/}
				IFS='-'; name=($result); unset IFS;
        name=${name//" "/}
				;;
    --host )
    		result="$@"
    		result=${result//"$1 "/}
    		IFS='-'; host=($result); unset IFS;
        host=${host//" "/}
    		;;
	esac
	shift
done


for container in `docker ps -q --filter "name=$name" --format "{{.Names}}"`; do
  docker exec $container bash -c echo "ServerName $host >> /etc/apache2/apache2.conf";
  docker restart $container
done
