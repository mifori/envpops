## Se obtienen los parametros del usuario
while [ "$1" != "" ]; do
	case $1 in
    --public )
				result="$@"
				result=${result//"$1 "/}
				IFS='-'; public=($result); unset IFS;
        public=${public//" "/}
				;;

    --private )
    		result="$@"
    		result=${result//"$1 "/}
    		IFS='-'; private=($result); unset IFS;
    		private=${private//" "/}
    		;;
	esac
	shift
done


if [ "$public" != "" ]; then
  docker exec -i git_server create_project gituser $public
elif [ "$private" != "" ]; then
  docker exec -i git_server create_project -p gituser $private
fi
