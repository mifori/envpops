Crear y cambiar de rama
```
git checkout -b newbranch
```

# Pull Request

1. Informar de que se quiere realizar una fusión entre un branch a otro

2.1. Visualmente realizar un ```git diff branch1..branch2``` para identificar los cambios
2.2. Aprobar o rechazar un PR

###### Rechazar
3. Notificar el rechazo del PR y el porque del rechazo

###### Aprobar
3. Realizar un ```git merge branch2 --no-ff -m "Merge branch 'branch2' of $NAME" -m "nombre de push" -m "Aprobado por $USER"```
4. Notificar la aprobacion del PR y notas del mismo

NAME = Puede ser la url donde existe el proyecto git o el numero del Pull Request

5. En caso de que se solicite eliminar la rama una vez fusionada usar ```git branch -D branch2```

En caso de conflictos se puede utilizar ```git merge --abort``` para volver a poner la rama en el estado antes del merge
