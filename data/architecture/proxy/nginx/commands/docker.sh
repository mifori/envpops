containers=$(docker ps -q --filter "name=_nginx" --format "{{.Names}}")

if [ "$containers" != "" ]; then
  docker restart $containers > /dev/null 2>&1
fi
