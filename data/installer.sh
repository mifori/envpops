#!/bin/sh
path=$HOME/containers

PROGRAM=envpops
VERSION=v0.2.4
FILE=${PROGRAM}-${VERSION}-linux-x64.zip

sudo yum install -y nano wget zip git-all

###Obtener parametros
while [ "$1" != "" ]; do
  case $1 in
  --path)
    result="$@"
    result=${result//"$1 "/}
    IFS='-'
    path=($result)
    unset IFS
    path=${path//" "/}
    ;;
  esac
  shift
done

### Descarga de envpops
wget https://envpops.io/installers/${FILE}
unzip ${FILE}
rm ${FILE}

### Instalar de forma global
if [[ -f "/usr/local/bin/envpops" ]]; then
  sudo rm /usr/local/bin/envpops
fi

if [[ -d "/etc/envpops" ]]; then
  sudo rm -r /etc/envpops
fi

sudo chmod a+x envpops
sudo mv envpops /usr/local/bin

### Definir carpeta del contenedor de los modulos envpops
echo 'export POPSPATH='$path >>~/.bashrc
source ~/.bashrc

#envpops install docker
#envpops init server --services proxy database
