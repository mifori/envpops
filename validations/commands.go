package validations

import (
	"os"

	"gitlab.com/mifori/envpops/console"
)

//Verify comprueba los comandos permitidos
func Verify(cmd string, args, permitted []string) ([]string, map[string][]string, []string) {
	commands, commandsArgs, initial := Extract(args)
	err := Validate(commands, permitted)

	if err != "" && commands[0] != "--help" {
		console.PrintI18nExitf("unknown/"+cmd, err)
		os.Exit(0)
	}

	if (len(commands) > 0 && commands[0] == "--help") || len(initial) == 0 {
		console.PrintI18nExitf("help/"+cmd, os.Getenv("POPSPROGRAM"))
		os.Exit(0)
	}

	return commands, commandsArgs, initial
}

//Check comprueba que el comando este correcto
func Check(args, permitted []string) ([]string, map[string][]string, []string, string) {
	commands, commandsArgs, initial := Extract(args)
	err := Validate(commands, permitted)

	return commands, commandsArgs, initial, err
}
