package validations

import "strings"

//Extract extrae los comandos - y -- con sus argumentos
func Extract(args []string) ([]string, map[string][]string, []string) {
	arguments := make(map[string][]string)
	commands := []string{}
	initial := []string{}

	for i := 0; i < len(args); i++ {
		arg := args[i]

		if strings.Index(arg, "--") == 0 {
			commands = append(commands, arg)
			arguments[arg] = []string{}
		} else if strings.Index(arg, "-") == 0 {
			commands = append(commands, arg)
			arguments[arg] = []string{}
		}

		length := len(commands)

		if length == 0 {
			initial = append(initial, arg)
			continue
		}

		value := commands[length-1]

		if value != arg {
			arguments[value] = append(arguments[value], arg)
		}
	}

	return commands, arguments, initial
}
