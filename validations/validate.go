package validations

//Validate  Verifica los comandos que pueden ser utilizados
func Validate(args []string, list []string) string {
	for i := 0; i < len(args); i++ {
		exist := false

		for j := 0; j < len(list); j++ {
			if args[i] == list[j] {
				exist = true
				break
			}
		}

		if !exist {
			return args[i]
		}
	}

	return ""
}
