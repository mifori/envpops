module gitlab.com/mifori/envpops

go 1.18

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/fatih/color v1.13.0
	github.com/tcnksm/go-gitconfig v0.1.2
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
)

require (
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/onsi/gomega v1.19.0 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/term v0.0.0-20201126162022-7de9c90e9dd1 // indirect
)
