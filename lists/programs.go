package lists

var Programs = map[string]bool{
	"jenkins":   true,
	"nginx":     true,
	"wordpress": true,

	"mariadb":  true,
	"mongo":    true,
	"mysql":    true,
	"postgres": true,
}
