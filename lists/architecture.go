package lists

var arch = map[string][]string{
	"automation": []string{"jenkins"},
	"backend":    []string{"gestor"},
	"container":  []string{"docker"},
	"database":   []string{"mysql", "mariadb", "postgres", "mongo"},
	"git":        []string{"local", "remote"},
	"proxy":      []string{"nginx"},

	"gestor":     []string{"wordpress"},
	"databaseWP": []string{"mysql", "mariadb"},
}

//Arch devuelve la lista de los comandos permitidos
func Arch(value string) []string {
	return arch[value]
}
