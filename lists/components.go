package lists

var Components = map[string]bool{
	"proxy":      true,
	"automation": true,
	"database":   true,
	"backend":    true,
}
