package main

import (
	"os"

	"gitlab.com/mifori/envpops/commands"
	"gitlab.com/mifori/envpops/config"
	"gitlab.com/mifori/envpops/console"
)

var list = map[string]func([]string){
	"git":     commands.Git,
	"init":    commands.Init,
	"rm":      commands.Deleted,
	"install": commands.Tools,
	"lang":    config.ChangeLanguage,
	"cert":    commands.SSLCertificate,

	"-v":        commands.Version,
	"--version": commands.Version,
}

func main() {
	config.Setup()

	args := os.Args[1:]

	if len(args) == 0 {
		console.PrintI18nExitf("help/commands", os.Getenv("POPSPROGRAM"))
	}

	startCommand(args)
}

//startCommand  verifica el comando solicitado y actúa
func startCommand(args []string) {
	arg := args[0]

	if list[arg] == nil {
		console.PrintI18nExitf("help/commands", os.Getenv("POPSPROGRAM"))
	}

	list[arg](args[1:])
}
