package execi

import (
	"bytes"
	"io"
	"os"
	"os/exec"
	"strings"
	"sync"
)

// Command  ejecuta los comandos
func Command(command string, s ...string) (string, error) {
	cmd := exec.Command(command, s...)
	return executeCommand(cmd, "")
}

// CommandDir  ejecuta los comandos en consola sin imprimir
func CommandDir(dir string, command string, s ...string) (string, error) {
	cmd := exec.Command(command, s...)
	cmd.Dir = dir
	return executeCommand(cmd, "")
}

// CommandShow  ejecuta los comandos y muestra su salida en consola
func CommandShow(command string, s ...string) (string, error) {
	cmd := exec.Command(command, s...)
	return executeCommand(cmd, "show")
}

// CommandShowDir  ejecuta los comandos en consola
func CommandShowDir(dir string, command string, s ...string) (string, error) {
	cmd := exec.Command(command, s...)
	cmd.Dir = dir
	return executeCommand(cmd, "show")
}

//executeCommand  Ejecuta el comando e imprime en consola
func executeCommand(cmd *exec.Cmd, action string) (string, error) {
	var stdoutBuf, stderrBuf bytes.Buffer
	stdoutIn, _ := cmd.StdoutPipe()
	stderrIn, _ := cmd.StderrPipe()

	var errStdout, errStderr error
	var stdout, stderr io.Writer

	if action == "show" {
		stdout = io.MultiWriter(os.Stdout, &stdoutBuf)
		stderr = io.MultiWriter(os.Stderr, &stderrBuf)
	} else {
		stdout = io.Writer(&stdoutBuf)
		stderr = io.Writer(&stderrBuf)
	}

	err := cmd.Start()
	if err != nil {
		return "", err
	}

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		_, errStdout = io.Copy(stdout, stdoutIn)
		wg.Done()
	}()

	_, errStderr = io.Copy(stderr, stderrIn)
	wg.Wait()

	err = cmd.Wait()
	if err != nil {
		return string(stderrBuf.Bytes()), err
	}

	if errStdout != nil || errStderr != nil {
		return string(stderrBuf.Bytes()), err
	}

	if action == "show" {
		return string(stdoutBuf.Bytes()), nil
	}

	return strings.TrimSpace(string(stdoutBuf.Bytes())), nil
}
