package tools

import (
	"bufio"
	"os"
	"regexp"
	"strings"
)

// Scan  obtiene lo escrito en consola
func Scan() string {
	in := bufio.NewReader(os.Stdin)
	read, _ := in.ReadString('\n')

	spacesLeadClose := regexp.MustCompile(`^[\s\p{Zs}]+|[\s\p{Zs}]+$`)
	spacesInside := regexp.MustCompile(`[\s\p{Zs}]{2,}`)
	final := spacesLeadClose.ReplaceAllString(read, "")
	final = spacesInside.ReplaceAllString(final, " ")

	return final
}

// Scanf  devuelve el primer valor del escaneo
func Scanf() string {
	scan := Scan()
	split := strings.Split(scan, " ")

	return split[0]
}
