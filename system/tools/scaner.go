package tools

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"gitlab.com/mifori/envpops/console"
)

//ScanerVersion imprime un mensaje y devuelve una respuesta del usuario
func ScanerVersion(folder string, program string) string {
	versions := readJSON(console.DirL(folder, "version"))
	texts := readJSON(console.DirLang(folder, "texts"))

	version := fmt.Sprintf("%v", versions[program])
	text := fmt.Sprintf("%v", texts["version"])

	fmt.Printf(text, version)
	scan := Scanf()

	if scan == "" {
		return version
	}

	return scan
}

//ScanerPort imprime un mensaje y devuelve una respuesta del usuario
func ScanerPort(folder string, program string) string {
	ports := readJSON(console.DirL(folder, "port"))
	texts := readJSON(console.DirLang(folder, "texts"))

	port := fmt.Sprintf("%v", ports[program])
	text := fmt.Sprintf("%v", texts["port"])

	fmt.Printf(text, port)
	scan := Scanf()

	if scan == "" {
		return port
	}

	return scan
}

//Scaner imprime un mensaje y devuelve una respuesta del usuario
func Scaner(folder string, program string, class string) string {
	ports := readJSON(console.DirL(folder, class))
	texts := readJSON(console.DirLang(folder, "texts"))

	port := fmt.Sprintf("%v", ports[program])
	text := fmt.Sprintf("%v", texts[program])

	fmt.Printf(text, port)
	scan := Scanf()

	if scan == "" {
		return port
	}

	return scan
}

//readJSON leer un archivo json
func readJSON(src string) map[string]interface{} {
	var result map[string]interface{}

	file, _ := ioutil.ReadFile(src + ".json")

	_ = json.Unmarshal([]byte(string(file)), &result)

	return result
}
