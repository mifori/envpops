package tools

import (
	"fmt"
	"strconv"

	"gitlab.com/mifori/envpops/console"
)

//SelectOption  obtiene la seleccion del usuario
func SelectOption(list []string) string {
	console.JSON("all", "select")
	opc := Scanf()

	num, err := strconv.Atoi(opc)
	if err != nil || num <= 0 || num > len(list) {
		console.JSONln("all", "void")
		return SelectOption(list)
	}

	selection := list[num-1]

	return selection
}

//Selection  obtiene la seleccion del usuario
func Selection(texts map[string]string, list []string) string {
	fmt.Println("\n" + texts["options"] + "\n")
	selection := selectionRecursive(texts, list)
	return selection
}

//selectRecursive   En caso de opcion incorrecta solicita otra entrada
func selectionRecursive(texts map[string]string, list []string) string {
	fmt.Print(texts["select"])

	opc := Scanf()

	num, err := strconv.Atoi(opc)
	if err != nil || num <= 0 || num > len(list) {
		fmt.Println(texts["invalid"])
		return selectionRecursive(texts, list)
	}

	return list[num-1]
}

//SelectionFile  obtiene la seleccion del usuario
func SelectionFile(texts map[string]string, list []string) string {
	fmt.Println("\n" + texts["options"] + "\n")

	console.Show(texts["options"])

	selection := selectionRecursive(texts, list)
	return selection
}
