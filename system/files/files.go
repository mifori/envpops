package files

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"

	"gitlab.com/mifori/envpops/system/cmdi"
)

//WalkAllFilesInDir lista todo el contenido de una carpeta
func WalkAllFilesInDir(dir string) error {
	return filepath.Walk(dir, func(path string, info os.FileInfo, e error) error {
		if e != nil {
			return e
		}

		if info.Mode().IsRegular() {
			fmt.Println("file name:", info.Name())
			fmt.Println("file path:", path)
		}

		return nil
	})
}

//ReplaceInFilei  reemplaza una cadena de texto dentro de un archivo
func ReplaceInFilei(file string, old string, new string) {
	if _, err := os.Stat(file); !os.IsNotExist(err) {
		ReplaceInFile(file, old, new)
	}
}

//ReplaceInFile  reemplaza una cadena de texto dentro de un archivo
func ReplaceInFile(file string, old string, new string) {
	tmp := os.Getenv("POPSTEMP") + "/replaces/"
	if _, err := os.Stat(tmp); os.IsNotExist(err) {
		cmdi.Mkdir(tmp)
	}

	cmdi.Copy(file, tmp)
	split := strings.Split(file, "/")
	name := split[len(split)-1]

	input, err := ioutil.ReadFile(tmp + name)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	output := bytes.Replace(input, []byte(old), []byte(new), -1)

	if err = ioutil.WriteFile(tmp+name, output, os.ModePerm); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	cmdi.Copy(tmp+name, file)
	cmdi.RemoveFile(tmp + name)
}

//ReplaceInDir  reemplaza una cadena de texto en los archivos de una carpeta
func ReplaceInDir(dir string, old string, new string) error {
	return filepath.Walk(dir, func(path string, info os.FileInfo, e error) error {
		if e != nil {
			return e
		}

		if strings.LastIndex(path, ".log") > 0 ||
			strings.LastIndex(path, ".pem") > 0 ||
			strings.LastIndex(path, ".key") > 0 {
			return nil
		}

		if info.Mode().IsRegular() {
			ReplaceInFile(path, old, new)
		}

		return nil
	})
}

//CopyFiles  copia los archivos de una carpeta a otra
func CopyFiles(dir, dst string) error {
	return filepath.Walk(dir, func(path string, info os.FileInfo, e error) error {
		if e != nil {
			return e
		}

		if info.Mode().IsRegular() {
			append := strings.Replace(dir, "./", "", -1)
			append = strings.Replace(path, append, "", -1)
			appendWithoutName := strings.Replace(append, info.Name(), "", -1)

			err := os.MkdirAll(dst+appendWithoutName, os.ModePerm)

			if err != nil {
				cmdi.MkdirAll(dst + appendWithoutName)
			}

			_, err = CopyFile(path, dst+append)

			if err != nil {
				cmdi.Copy(path, dst+append)
			}
		}

		return nil
	})
}

//CopyFile  copia un archivo
func CopyFile(src, dst string) (int64, error) {
	//os.MkdirAll(dst, os.ModePerm)

	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

// ReadVariables  devuelve las variables en una cadena de texto
func ReadVariables(structure interface{}) string {
	d := reflect.ValueOf(structure).Elem()
	val := reflect.Indirect(d)
	var textFile string

	for i := 0; i < d.NumField(); i++ {
		key := strings.ToUpper(val.Type().Field(i).Name)
		value := convertByType(d.Field(i))
		textFile += key + "=" + value + "\n"
	}

	return textFile
}

//WriteVariables  escribe un texto dentro de un archivo
func WriteVariables(file, textFile string) {
	writeFile := []byte(textFile)
	err := ioutil.WriteFile(file, writeFile, 0644)

	if err != nil {
		panic(err)
	}
}

//WriteInclude  escribe un archivo incluyendo un texto al final
func WriteInclude(file string, text string, i interface{}) {
	textFile := ReadVariables(i)
	textFile += text
	WriteVariables(file, textFile)
}

//convertByType  convierte el dato leido a su tipo de estructura y lo devuelve como string
func convertByType(field reflect.Value) string {
	out := ""

	switch field.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		out = strconv.FormatInt(field.Int(), 10)
	case reflect.Float32, reflect.Float64:
		out = strconv.FormatFloat(field.Float(), 'f', -1, 64)
	case reflect.Bool:
		out = strconv.FormatBool(field.Bool())
	case reflect.String:
		out = field.String()
	}

	return out
}

var targetDir = "./"

//Unzip  descomprime un archivo .zip
func Unzip(file string) {
	zipReader, _ := zip.OpenReader(file)
	for _, file := range zipReader.Reader.File {

		zippedFile, err := file.Open()
		if err != nil {
			log.Fatal(err)
		}
		defer zippedFile.Close()

		//targetDir := "./"

		extractedFilePath := filepath.Join(
			targetDir,
			file.Name,
		)

		if file.FileInfo().IsDir() {
			log.Println("Directory Created:", extractedFilePath)
			os.MkdirAll(extractedFilePath, file.Mode())
		} else {
			log.Println("File extracted:", file.Name)

			outputFile, err := os.OpenFile(
				extractedFilePath,
				os.O_WRONLY|os.O_CREATE|os.O_TRUNC,
				file.Mode(),
			)
			if err != nil {
				log.Fatal(err)
			}
			defer outputFile.Close()

			_, err = io.Copy(outputFile, zippedFile)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}

//UnzipDir  descomprime en una ubicacion especifica
func UnzipDir(file, dir string) {
	targetDir = dir
	Unzip(file)
	targetDir = "./"
}

//UnzipDirVoid  descomprime en una ubicacion especifica
func UnzipDirVoid(file, dir string) {
	targetDir = dir
	UnzipVoid(file)
	targetDir = "./"
}

//UnzipVoid  descomprime un archivo .zip
func UnzipVoid(file string) {
	zipReader, _ := zip.OpenReader(file)
	for _, file := range zipReader.Reader.File {

		zippedFile, err := file.Open()
		if err != nil {
			log.Fatal(err)
		}
		defer zippedFile.Close()

		extractedFilePath := filepath.Join(
			targetDir,
			file.Name,
		)

		if file.FileInfo().IsDir() {
			os.MkdirAll(extractedFilePath, file.Mode())
		} else {
			outputFile, err := os.OpenFile(
				extractedFilePath,
				os.O_WRONLY|os.O_CREATE|os.O_TRUNC,
				file.Mode(),
			)
			if err != nil {
				log.Fatal(err)
			}
			defer outputFile.Close()

			_, err = io.Copy(outputFile, zippedFile)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}
