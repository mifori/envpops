package cmdi

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	humanize "github.com/dustin/go-humanize"
)

//DownloadFileProd  descarga un archivo en caso de estar en produccion
func DownloadFileProd(url string, path string, file string) error {
	if os.Getenv("POPSMODE") == "dev" {
		return nil
	}

	return DownloadFile(url, path, file)
}

//DownloadFileA  busca y descarga un archivo
func DownloadFileA(filepath string, url string) error {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

//DownloadFile  busca y descarga un archivo
func DownloadFile(url string, path string, file string) error {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New("Received non 200 response code")
	}

	// Create the file
	POPSTEMP := os.Getenv("POPSTEMP") + "/"
	out, err := os.Create(POPSTEMP + file)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	MkdirAll(path)

	if POPSTEMP+file != path+file {
		Move(POPSTEMP+file, path+file)
	}

	return nil
}

// WriteCounter counts the number of bytes written to it. It implements to the io.Writer
// interface and we can pass this into io.TeeReader() which will report progress on each
// write cycle.
type WriteCounter struct {
	Total uint64
}

func (wc *WriteCounter) Write(p []byte) (int, error) {
	n := len(p)
	wc.Total += uint64(n)
	wc.PrintProgress()
	return n, nil
}

//PrintProgress  imprime en consola
func (wc WriteCounter) PrintProgress() {
	// Clear the line by using a character return to go back to the start and remove
	// the remaining characters by filling it with spaces
	fmt.Printf("\r%s", strings.Repeat(" ", 35))

	// Return again and print current status of download
	// We use the humanize package to print the bytes in a meaningful way (e.g. 10 MB)
	fmt.Printf("\rDownloading... %s complete", humanize.Bytes(wc.Total))
}

//WgetIntern  inicia la desacarga de un archivo
/*func WgetIntern(url string) {
	split := strings.Split(url, "/")
	file := split[len(split)-1]

	err := DownloadFileP(file, url)
	if err != nil {
		panic(err)
	}

	fmt.Println("Download Finished")
}*/

// DownloadFileP will download a url to a local file. It's efficient because it will
// write as it downloads and not load the whole file into memory. We pass an io.TeeReader
// into Copy() to report progress on the download.
func DownloadFileP(url string, path string, file string) error {
	POPSTEMP := os.Getenv("POPSTEMP") + "/"
	// Create the file, but give it a tmp file extension, this means we won't overwrite a
	// file until it's downloaded, but we'll remove the tmp extension once downloaded.
	out, err := os.Create(POPSTEMP + file + ".tmp")
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New("Received non 200 response code")
	}

	// Create our progress reporter and pass it to be used alongside our writer
	counter := &WriteCounter{}
	_, err = io.Copy(out, io.TeeReader(resp.Body, counter))
	if err != nil {
		return err
	}

	// The progress use the same line so print a new line once it's finished downloading
	fmt.Print("\n")

	err = os.Rename(POPSTEMP+file+".tmp", POPSTEMP+file)
	if err != nil {
		return err
	}

	MkdirAll(path)

	if POPSTEMP+file != path+file {
		Move(POPSTEMP+file, path+file)
	}

	return nil
}

//DownloaderZip descarga y descomprime un zip
func DownloaderZip(src string, dest string) error {
	POPSTEMP := os.Getenv("POPSTEMP") + "/"
	SERVER := os.Getenv("POPSSERVER")

	if _, err := os.Stat(dest); !os.IsNotExist(err) {
		RemoveFolder(dest)
	}

	file := src + ".zip"
	split := strings.Split(src, "/")
	name := split[len(split)-1] + ".zip"

	if os.Getenv("POPSMODE") != "dev" {
		stderr := DownloadFileP(SERVER+file, POPSTEMP, name)

		if stderr != nil {
			return stderr
		}

		UnzipS(POPSTEMP+name, POPSTEMP)

		splitDest := strings.Split(dest, "/")
		splitDest = splitDest[:len(splitDest)-1]

		MkdirAll(strings.Join(splitDest[:], "/"))
		Move(POPSTEMP+split[len(split)-1]+"/", dest)
		RemoveFile(POPSTEMP + name)
	}

	return nil
}
