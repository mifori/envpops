package cmdi

import (
	"log"

	"gitlab.com/mifori/envpops/so"
	"gitlab.com/mifori/envpops/system/execi"
)

//Exec ejecuta un comando
func Exec(command string, params ...string) {
	executeExec(execi.CommandShow, command, params...)
}

//ExecI ejecuta un comando sin mostrar en consola
func ExecI(command string, params ...string) {
	executeExec(execi.Command, command, params...)
}

//ExecE devuelve el error de la ejecución sin detener el sistema
func ExecE(command string, params ...string) (string, error) {
	_, err := execi.Command(command, params...)

	if err == nil {
		return "", nil
	}

	perm := so.Conf("permission")
	params = append([]string{command}, params...)
	stderr, err := execi.Command(perm, params...)

	if err != nil {
		return stderr, err
	}

	return "", nil
}

//ExecP devuelve el error de la ejecución sin detener el sistema
func ExecP(command string, params ...string) {
	perm := so.Conf("permission")
	params = append([]string{command}, params...)
	execi.Command(perm, params...)
}

//ExecG devuelve el error de la ejecución sin detener el sistema
func ExecG(command string, params ...string) string {
	permission := so.Conf("group", command)

	if permission == "" {
		resp, _ := execi.Command(command, params...)
		return resp
	}

	params = append([]string{command}, params...)
	resp, _ := execi.Command(permission, params...)
	return resp
}

//ExecShow ejecuta un comando y muestra su resulta en la consola
func ExecShow(command string, params ...string) (string, error) {
	stderr, err := execi.CommandShow(command, params...)

	if err != nil {
		return stderr, err
	}

	return "", nil
}

//executeExec verifica permisos e inicia el commando a ejecutar
func executeExec(
	action func(string, ...string) (string, error),
	command string,
	params ...string,
) {
	_, err := execi.Command(command, params...)

	if err == nil {
		return
	}

	perm := so.Conf("permission")
	params = append([]string{command}, params...)
	strerr, err := action(perm, params...)

	if err != nil {
		log.Fatal(strerr)
	}
}
