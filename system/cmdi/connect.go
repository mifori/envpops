package cmdi

import (
	"fmt"
	"net/http"
	"os"
)

//Connected  determina si tiene conexion a internet
func Connected() (ok bool) {
	_, err := http.Get("http://clients3.google.com/generate_204")

	if err != nil {
		return false
	}

	return true
}

//IsConnected  detiene la aplicacion cuando requiera acceso a internet
func IsConnected() {
	approve := Connected()

	if !approve {
		fmt.Println("No tienes conexion a internet.")
		os.Exit(0)
	}
}
