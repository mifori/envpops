package cmdi

import "os"

//Chown cambia los dueños del archivo o carpeta
func Chown(params ...string) {
	Exec("chown", params...)
}

//Chmod cambia los permisos
func Chmod(params ...string) {
	Exec("chmod", params...)
}

//Copy copia un archivo
func Copy(params ...string) {
	Exec("cp", params...)
}

//Copyi copia un archivo
func Copyi(params ...string) {
	if _, err := os.Stat(params[0]); !os.IsNotExist(err) {
		Exec("cp", params...)
	}
}

//Remove elimina archivos o carpetas
func Remove(params ...string) {
	Exec("rm", params...)
}

//Mkdir crea una nueva carpeta
func Mkdir(src string) {
	if _, err := os.Stat(src); os.IsNotExist(err) {
		Exec("mkdir", src)
	}
}

//MkdirAll crea una nueva carpeta y sus carpetas padres de no existir
func MkdirAll(src string) {
	Exec("mkdir", "-p", src)
}

//Move mueve un archivo o carpeta
func Move(crt, dst string) {
	Exec("mv", crt, dst)
}

//RemoveFile elimina un archivo
func RemoveFile(src string) {
	Exec("rm", src)
}

//RemoveFolder elimina una carpeta
func RemoveFolder(src string) {
	Exec("rm", "-r", src)
}

//Wget  descarga archivos
func Wget(params ...string) {
	Exec("wget", params...)
}

//WgetI  descarga archivos sin mostrar los mensajes en consola
func WgetI(params ...string) {
	ExecI("wget", params...)
}

//UnzipI  descarga archivos sin mostrar los mensajes en consola
func UnzipI(params ...string) {
	ExecI("unzip", params...)
}
