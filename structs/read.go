package structs

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"gitlab.com/mifori/envpops/system/cmdi"
)

//read  Lee y devuelve la estructura de un archivo json
func read(src string, v interface{}) {
	file, _ := ioutil.ReadFile(src)
	_ = json.Unmarshal([]byte(file), v)
}

//write  Escribe estructura dentro de un archivo json
func write(name, path string, data interface{}) {
	tmp := os.Getenv("POPSTEMP") + "/" + name

	file, _ := json.MarshalIndent(data, "", " ")
	_ = ioutil.WriteFile(tmp, file, 0644)

	if _, err := os.Stat(path); !os.IsNotExist(err) {
		cmdi.RemoveFile(path)
	}

	cmdi.Move(tmp, path)
}
