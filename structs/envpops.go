package structs

import "os"

//Envpops  Define la estructura del json
type Envpops struct {
	Name    string
	Project string
	Version string

	Architecture envpopsArchitecture
	Server       envpopsServer
	Databases    []string

	Repository     string
	RepositoryType string
}

type envpopsServer struct {
	Domain   string
	HTTPS    bool
	Provider string
}

type envpopsArchitecture struct {
	Automation string
	Container  string
	Proxy      string
}

//GetConfig  busca y devuelve el contenido del archivo json
func GetConfig() Envpops {
	data := Envpops{}
	read(os.Getenv("POPSFOLDER")+"/"+os.Getenv("POPSPROGRAM")+".json", &data)
	return data
}

//SetConfig  cambia o agrega contenido al json
func SetConfig(data Envpops) {
	pathFile := os.Getenv("POPSFOLDER") + "/" + os.Getenv("POPSPROGRAM") + ".json"
	write(os.Getenv("POPSPROGRAM")+".json", pathFile, data)
}
