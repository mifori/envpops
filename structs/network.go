package structs

import "os"

//Network  define la estructura del json
type Network struct {
	Proxy netProxy
	IP    netIps
}

type netProxy struct {
	RED_GIT   string
	RED_FRONT string
	RED_BACK  string
}

type netIps struct {
	Back     string
	Front    string
	Git      string
	Back_SP  []int64
	Front_SP []int64
	Git_SP   []int64
}

//GetNetwork  busca y devuelve la configuracion de la red
func GetNetwork() Network {
	data := Network{}
	read(os.Getenv("POPSDATA")+"/network.json", &data)
	return data
}
