package structs

import "os"

//Config  define la estructura del json
type Config struct {
	Language       string
	Container      string
	Repository     string
	RepositoryType string
}

//GetConfigGlobal  Busca y devuelve el contenido del archivo json
func GetConfigGlobal() Config {
	data := Config{}
	read(os.Getenv("POPSCONF")+"/config.json", &data)
	return data
}

//SetConfigGlobal  Cambia o agrega contenido al json
func SetConfigGlobal(data Config) {
	pathFile := os.Getenv("POPSCONF") + "/config.json"
	write("config.json", pathFile, data)
}
